{-# LANGUAGE OverloadedStrings #-}

import Common.Confusion
import           Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import System.Exit
import Data.Monoid
import Data.Text (Text)
import Data.List


confusion :: MonadGen m => m t -> m (ConfMatrix t)
confusion v = ConfMatrix <$> v <*> v <*> v <*> v

kappa_range_int64 :: Property
kappa_range_int64 =
  property $ do
    cm <- forAll $ confusion $ Gen.int64 (Range.linear 0 10000)
    let kcm = kappa cm
    let orNaN f = \ k v -> k /= k || f k v
    diff kcm (orNaN (<=)) (1.0)
    diff kcm (orNaN (>=)) (-1.0)

kappa_range_sum64 :: Property
kappa_range_sum64 =
  property $ do
    cm <- forAll $ confusion $ Sum <$> Gen.int64 (Range.linear 0 10000)
    let kcm = kappa $ getSum <$> cm
    let orNaN f = \ k v -> k /= k || f k v
    diff kcm (orNaN (<=)) (1.0)
    diff kcm (orNaN (>=)) (-1.0)

sublist :: (Foldable f, Eq a) => f a -> f a -> Bool
sublist a b = all (`elem` b) a


tests :: IO Bool
tests = do
  checkParallel $ Group "Api Tests" [ ("kappa_range_int64", kappa_range_int64)
                                    , ("kappa_range_sum64", kappa_range_sum64)
                                    ]

main :: IO ()
main = do
  result <- tests
  if result
    then return ()
    else exitFailure
