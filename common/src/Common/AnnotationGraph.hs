{-# LANGUAGE TypeApplications #-}

module Common.AnnotationGraph where

import Common.Pattern

import Control.Lens hiding (pre)

import Data.Graph.Inductive hiding ((&))
import Data.Map hiding (null, filter)
import Data.Coerce
import Data.Int

type AnnotChain = Gr () ()


node :: Iso' PatternId Node
node = iso (fromIntegral . coerce @PatternId @Int64) (coerce @Int64 @PatternId . fromIntegral)

annotChainFromList :: [(PatternId, PatternId)] -> AnnotChain
annotChainFromList edgs = mkGraph lnodes ledges
  where
    lnodes = [ (n, ()) | n <- edgs ^.. traversed . each . node ]
    ledges = [ (f ^. node, t ^. node, ()) | (f, t) <- edgs ]

pathsFrom :: PatternId -> Gr a b -> [[PatternId]]
pathsFrom p g = if null $ suc g (p ^. node) then [[p]] else [ p : rest | s <- suc g (p ^. node) ^.. traversed . from node, rest <- pathsFrom s g]

pathsTo :: PatternId -> Gr a b -> [[PatternId]]
pathsTo p g =  if null $ pre g (p ^. node) then [[p]] else [ p : rest | s <- pre g (p ^. node) ^.. traversed . from node, rest <- pathsTo s g]

enrich :: Monoid a => Map PatternId a -> AnnotChain -> Gr a ()
enrich m = (gmap $ \ (a1, n, (), a2) -> (a1, n, m ^. ix (n ^. from node), a2)) . insNewNodes ((toList m) & mapped . _1 %~ view node & mapped . _2 .~ ())
  where
    insNewNodes ns g = insNodes (filter (not . flip gelem g . view _1) ns) g

lpathsFrom :: Monoid a => PatternId -> Gr a b -> [[(PatternId, a)]]
lpathsFrom p g = [ [ (n, (lab g (n ^. node)) ^. _Just) | n <- path ] | path <- pathsFrom p g ]

lpathsTo :: Monoid a => PatternId -> Gr a b -> [[(PatternId, a)]]
lpathsTo p g = [ [ (n, (lab g (n ^. node)) ^. _Just) | n <- path ] | path <- pathsTo p g ]
