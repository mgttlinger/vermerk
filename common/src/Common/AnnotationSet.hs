{-# LANGUAGE DeriveAnyClass, DeriveFunctor, DeriveFoldable, DeriveTraversable, DeriveGeneric, TemplateHaskell, DerivingStrategies, GeneralisedNewtypeDeriving, DeriveGeneric #-}

module Common.AnnotationSet where

import Control.Lens.TH

import Data.Aeson
import Data.Text

import GHC.Generics

newtype AnnotationSetId = AnnSetId Text deriving newtype (Show, Eq, Ord, FromJSON, ToJSON)
                                        deriving stock Generic

$(makePrisms 'AnnSetId)
