{-# LANGUAGE TemplateHaskell, PatternSynonyms, DerivingStrategies, GeneralisedNewtypeDeriving, DeriveGeneric #-}

module Common.Pattern where

import Common.Helpers

import Control.Lens

import Data.Text
import Data.Int
import Data.Aeson
import Data.Aeson.TH
import Data.List
import Data.Either

import GHC.Generics (Generic)

import Text.Trifecta hiding (decimal)

-- | A pattern represents a class to be assigned to the annotated content
data Pattern = Pattern { _name :: Text -- ^ what the pattern should be called
                       , _template :: Text -- ^ formula template containing placeholders. See slotParser for explanation of their formating
                       , _explanation :: Text -- ^ short explanation what the pattern is supposed to mean
                       , _retired :: Bool -- ^ wether the pattern is no longer in use
                       } deriving (Show, Eq, Generic)

$(makeLenses ''Pattern)
$(deriveJSON shortOptions ''Pattern)

-- | Patterns are simply numbered
newtype PatternId = PatternId Int64 deriving newtype (Eq, Show, Read, Ord, ToJSON, FromJSON, ToJSONKey, FromJSONKey)

$(makePrisms 'PatternId)

-- | A slot represents a placeholder in a pattern explanation
data Slot = Slot { _slotName :: Text -- ^ the name of the slot or the referred to name if this is a slot reference
                 , _meta :: Maybe Text -- ^ the type of the slot. Nothing for slot references
                 } deriving (Show, Eq)

$(makeLenses ''Slot)

pattern SlotRef :: Text -> Slot
pattern SlotRef n = Slot n Nothing

-- | Checks whether the slot is a slot reference
isRef :: Slot -> Bool
isRef (SlotRef _) = True
isRef _ = False

-- | Parses slots from a pattern template of the shape {?name : type} and references to placeholders of the shape {?name}
slotParser :: Parser Slot
slotParser = do
  _ <- string "{?"
  _ <- spaces
  res <- some $ alphaNum
  _ <- spaces
  m <- option Nothing $ Just <$> do
    _ <- char ':'
    _ <- spaces
    some $ satisfy ('}' /=)
  _ <- char '}'
  return $ Slot (pack res) (pack <$> m)

-- | Parses names from a known list of slot names into solt references. Used for augmenting pattern explanations for later interleaving of slot fillers
knownSlotParser :: [String] -> Parser Slot
knownSlotParser names = SlotRef <$> choice (fmap pack . string <$> names)

-- | Typed representation of a pattern template or explanation as Text with Slots in between
type Template = [Either Text Slot]

templateParser' :: Parser Slot -> Parser Template
templateParser' sParser = mergeLefts <$> do
  res <- many $ choice [ Right <$> sParser
                       , Left . pack . pure <$> anyChar
                       ]
  eof
  return res
  where
    mergeLefts [] = []
    mergeLefts (a : []) = [a]
    mergeLefts (Left a : Left b : c) = mergeLefts $ (Left $ a <> b) : c
    mergeLefts (a : b) = a : mergeLefts b

templateParser :: Parser Template
templateParser = templateParser' slotParser

explanationParser :: [String] -> Parser Template
explanationParser = templateParser' . knownSlotParser

parseTemplate :: Text -> Either String Template
parseTemplate = parseText templateParser
parseExplanation :: [String] -> Text -> Either String Template
parseExplanation sNames = parseText $ explanationParser sNames

augmentText :: [String] -> Text -> Template
augmentText sNames t = recM $ parseExplanation sNames t

slotExpl :: Pattern -> Template
slotExpl pat = let tpl = recM $ parseTemplate $ _template pat
                   sNames = nub $ fmap (unpack . _slotName) $ rights $ tpl
               in augmentText sNames $ _explanation pat

slotNames :: Pattern -> [Text]
slotNames = nub . fmap _slotName . rights . recM . parseTemplate . _template
