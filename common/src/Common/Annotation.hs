{-# LANGUAGE LambdaCase, TemplateHaskell, DerivingVia, DeriveGeneric #-}

module Common.Annotation where

import Common.Helpers
import Common.Range

import Control.Applicative
import Control.Lens

import Data.Aeson (FromJSON, ToJSON)
import Data.Text (Text)
import Data.Map
import Data.Maybe
import Data.List
import Data.Int

import GHC.Generics

-- | Constructive state of an annotation
data AnnotationState = Annotated -- ^ annotated as true positive
                     | NegAnnotated -- ^ annotated as true negative
                     | ImplAnnotated -- ^ annotated implicitly by specialisation
                     | ImplNegAnnotated -- ^ annotated negative implicitly by specialisation
                     deriving stock (Eq, Ord, Show, Enum, Bounded)
                     deriving (FromJSON, ToJSON) via (ViaEnum AnnotationState)

isImplicit :: AnnotationState -> Bool
isImplicit ImplAnnotated = True
isImplicit ImplNegAnnotated = True
isImplicit _ = False


-- | 'AnnotationState' can be seen as 'Bool'
annStateBool :: Iso' AnnotationState Bool
annStateBool = iso (\ as -> Annotated == as || ImplAnnotated == as) (\case
                                      True -> Annotated
                                      False -> NegAnnotated)

oppAnnot :: (Profunctor p, Contravariant f) => Optic' p f AnnotationState AnnotationState
oppAnnot = Control.Lens.to (over annStateBool not)

-- | Reduces a list of annotations to the one t oshow to the user in case there are multiple
displayAnnotation :: [AnnotationState] -> AnnotationState
displayAnnotation = fromJust . (\ as -> (find (not . isImplicit) as <|> as ^? _head))

type Author = Text

data Annotation = Ann { _aManual :: Bool -- ^ is the '_annotator' a human?
                      , _aState :: AnnotationState -- ^ the kind of annotation i.e. positive or negative
                      , _aAuthor :: Author
                      , _aComment :: Maybe Text
                      } deriving (Eq, Show, Generic)

$(makeLenses ''Annotation)
$(deriveShortJSON ''Annotation)

-- | A gold annotation is an annotation with some additional fields
data GoldAnnotation = GAnn { _gState :: AnnotationState -- ^ the kind of annotation i.e. positive or negative
                           , _gTopLevel :: Maybe Bool -- ^ whether this annotation captures the outermost level
                           , _gUndecidable :: Bool -- ^ whether it was determined that it is undecidable what the annotation should be but leaning towards the annotation made here
                           , _gAuthor :: Author
                           , _gComment :: Maybe Text
                           } deriving (Eq, Show, Generic)

$(makeLenses ''GoldAnnotation)
$(deriveShortJSON ''GoldAnnotation)

class IsAnnotation t  where
  annotationState :: Lens' t AnnotationState
  comment :: Lens' t (Maybe Text)
  author :: Lens' t Author
  implicit :: Getter t Bool
  implicit = annotationState . Control.Lens.to (\ s -> ImplAnnotated == s || ImplNegAnnotated == s)

instance IsAnnotation Annotation where
  annotationState = aState
  comment = aComment
  author = aAuthor

instance IsAnnotation GoldAnnotation where
  annotationState = gState
  comment = gComment
  author = gAuthor

asAnnotation :: GoldAnnotation -> Annotation
asAnnotation ga = Ann True (ga ^. annotationState) (ga ^. author) (ga ^. comment)


lookupFromTuples :: (Ord k) => [(k,t)] -> Map k [t]
lookupFromTuples = collectIntoMap fst (pure . snd)

makeLookup :: (Ord c, Ord a) => [(c, (Text, (a, Annotation)))] -> Map c (Map Text (Map a [Annotation]))
makeLookup = fmap (fmap lookupFromTuples . lookupFromTuples) . lookupFromTuples

makeGoldLookup :: (Ord c, Ord a) => [(c, (a, GoldAnnotation))] -> Map c (Map a [GoldAnnotation])
makeGoldLookup = fmap lookupFromTuples . lookupFromTuples

type SlotAnnotation = Map Text (Range Int32) -- ^ the slot annotations
