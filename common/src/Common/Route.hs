{-# LANGUAGE EmptyCase #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
module Common.Route where

import Common.Settings
import Common.AnnotationSet

import Prelude hiding (id, (.))

import Data.Text (Text)
import Data.Functor.Identity
import Data.Map

import Control.Category
import Control.Monad.Except
import Control.Lens

import Obelisk.Route
import Obelisk.Route.TH

data BackendRoute :: * -> * where
  BackendRoute_Missing :: BackendRoute () -- ^ Used to handle unparseable routes
  BackendRoute_Api :: BackendRoute () -- ^ Used for API calls

data AnnotRoute :: * -> * where
  Annot :: AnnotRoute AnnotationSettings -- ^ Used for creating annotations
  Adjud :: AnnotRoute AnnotationSettings -- ^ Used for creating the gold standard from the annotations
  Regions :: AnnotRoute AnnotationSettings -- ^ Used for region annotation
  Refinement :: AnnotRoute AnnotationSettings -- ^ Used for annotation refinement

data FrontendRoute :: * -> * where
  FrontendRoute_Main :: FrontendRoute () -- ^ Used for the landing page
  FrontendRoute_Tweets :: FrontendRoute (R AnnotRoute) -- ^ Used for Tweet annotation
  FrontendRoute_Germa :: FrontendRoute (R AnnotRoute) -- ^ Used for Germaparl annotation

listEncoder :: (Read b, Show b, Applicative check, MonadError Text parse) => Iso' b a -> Encoder check parse [a] Text
listEncoder elep = unsafeTshowEncoder . reviewEncoder (mapping elep)

annotationSetEncoder :: (MonadError Text parse, Applicative check) => Encoder check parse AnnotationSetFilter Text
annotationSetEncoder = reviewEncoder $ iso (
  \case
    "all" -> AnnotationSetFilter $ Nothing
    t -> AnnotationSetFilter $ _Just . coerced # t
   )(
  \case
    AnnotationSetFilter Nothing -> "all"
    AnnotationSetFilter (Just t) -> t ^. coerced
  )

-- annotationSetFilterEncoder :: (MonadError Text parse, MonadError Text check, check ~ parse) => Encoder check parse AnnotationSetFilter (Map Text (Maybe Text))
-- annotationSetFilterEncoder = dmapEncoder k v . fieldMapEncoder
--   where
--     --k :: Encoder check parse (Some AnnotationSettingsField) Text
--     k = enum1Encoder $ \case
--       AnnotationSetFilter_Set -> "set"
--     v :: forall c p a. (MonadError Text p, MonadError Text c, c ~ p) => AnnotationSetFilterField a -> Encoder c p a (Maybe Text)
--     v f = case f of
--       AnnotationSetFilter_Set -> maybeEncoder nothingEncoder justEncoder

patternFilterEncoder :: (MonadError Text parse, MonadError Text check, check ~ parse) => Encoder check parse PatternFilter (Map Text (Maybe Text))
patternFilterEncoder = dmapEncoder k v . fieldMapEncoder
  where
    --k :: Encoder check parse (Some AnnotationSettingsField) Text
    k = enum1Encoder $ \case
      PatternFilter_Patterns -> "patterns"
    v :: forall c p a. (MonadError Text p, MonadError Text c, c ~ p) => PatternFilterField a -> Encoder c p a (Maybe Text)
    v f = case f of
      PatternFilter_Patterns -> justEncoder . listEncoder id

annotRouteEncoder :: (MonadError Text parse, MonadError Text check, check ~ parse) => Encoder check parse (R AnnotRoute) PageName
annotRouteEncoder = pathComponentEncoder
  (\case
      Annot -> PathSegment "annotate" $ pathParamEncoder annotationSetEncoder $ queryOnlyEncoder . patternFilterEncoder
      Adjud -> PathSegment "adjudicate" $ pathParamEncoder annotationSetEncoder $ queryOnlyEncoder . patternFilterEncoder
      Regions -> PathSegment "regions" $ pathParamEncoder annotationSetEncoder $ queryOnlyEncoder . patternFilterEncoder
      Refinement -> PathSegment "refine" $ pathParamEncoder annotationSetEncoder $ queryOnlyEncoder . patternFilterEncoder
  )

fullRouteEncoder
  :: Encoder (Either Text) Identity (R (FullRoute BackendRoute FrontendRoute)) PageName
fullRouteEncoder = mkFullRouteEncoder
  (FullRoute_Backend BackendRoute_Missing :/ ())
  (\case
      BackendRoute_Missing -> PathSegment "missing" $ unitEncoder mempty
      BackendRoute_Api -> PathSegment "api" $ unitEncoder mempty
  )
  (\case
      FrontendRoute_Main -> PathEnd $ unitEncoder mempty
      FrontendRoute_Tweets -> PathSegment "tweets" $ annotRouteEncoder
      FrontendRoute_Germa -> PathSegment "germa" $ annotRouteEncoder
  )

-- derive instance things for obelisk inner workings of routing
concat <$> mapM deriveRouteComponent
  [ ''BackendRoute
  , ''FrontendRoute
  , ''AnnotRoute
  ]
