module Common.Range where

import Control.Applicative

import Text.Trifecta

type Range t = (t, t)

rangeParser :: Num t => Parser (Range t)
rangeParser = do
  ob <- True <$ char '(' <|> False <$ char '['
  spaces
  s <- fromInteger <$> integer
  spaces
  _ <- comma
  spaces
  e <- fromInteger <$> integer
  cb <- True <$ char ')' <|> False <$ char ']'
  return (if ob then s + 1 else s, if cb then e - 1 else e)
