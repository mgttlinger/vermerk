{-# LANGUAGE GADTs, TemplateHaskell, KindSignatures, FlexibleInstances, FlexibleContexts, MultiParamTypeClasses, TypeFamilies #-}

module Common.Auth where

import Control.Lens

import Data.Aeson

data Login = Login { _user :: String
                   , _password :: String
                   } deriving (Eq)

$(makeLenses 'Login)

instance ToJSON Login where
  toJSON (Login u p) = toJSON (u, p)

instance FromJSON Login where
  parseJSON v = do
    (u, p) <- parseJSON v
    pure $ Login u p

-- | Show instance for 'Login' hides the password to prevent accidentally exposing it in logs and the like
instance Show Login where
  show (Login usr _) = "Login: " <> usr
