{-# LANGUAGE TemplateHaskell, GADTs, KindSignatures, TypeFamilies #-}

module Common.Settings where

import Common.Helpers
import Common.Pattern
import Common.AnnotationSet

import Data.Aeson.TH
import Data.Default
import Data.Maybe
import Data.Tabulation
import Data.GADT.Show.TH
import Data.GADT.Compare.TH
import Data.Universe.Some.TH

import Control.Lens

class Filter t where
  filters :: Getter t Bool

newtype PatternFilter = PatternFilter { _patterns :: [PatternId]
                                      } deriving (Show, Eq)

$(deriveJSON shortOptions ''PatternFilter)
$(makeLenses ''PatternFilter)

instance Default PatternFilter where
  def = PatternFilter []

instance Filter PatternFilter where
  filters = to $ nullOf $ patterns . folded

patternFilter :: PatternFilter -> (PatternId -> Bool)
patternFilter tas = if null $ _patterns tas
                       then const True
                       else flip elem $ _patterns tas

data PatternFilterField :: * -> * where
    PatternFilter_Patterns :: PatternFilterField [PatternId]

-- Derive boilerplate to render this GADT over the wire as part of the route
$(deriveGEq ''PatternFilterField)
$(deriveGCompare ''PatternFilterField)
$(deriveGShow ''PatternFilterField)
$(deriveUniverseSome ''PatternFilterField)

instance HasFields PatternFilter where
  type Field PatternFilter = PatternFilterField

  fieldLens PatternFilter_Patterns = patterns

  tabulateFieldsA f = PatternFilter <$> f PatternFilter_Patterns


newtype AnnotationSetFilter = AnnotationSetFilter { _annset :: Maybe AnnotationSetId
                                                  } deriving (Show, Eq)

$(deriveJSON shortOptions ''AnnotationSetFilter)
$(makeLenses ''AnnotationSetFilter)

instance Filter AnnotationSetFilter where
  filters = to $ isJust . _annset

instance Default AnnotationSetFilter where
  def = AnnotationSetFilter Nothing

data AnnotationSetFilterField :: * -> * where
    AnnotationSetFilter_Set :: AnnotationSetFilterField (Maybe AnnotationSetId)

-- Derive boilerplate to render this GADT over the wire as part of the route
$(deriveGEq ''AnnotationSetFilterField)
$(deriveGCompare ''AnnotationSetFilterField)
$(deriveGShow ''AnnotationSetFilterField)
$(deriveUniverseSome ''AnnotationSetFilterField)

instance HasFields AnnotationSetFilter where
  type Field AnnotationSetFilter = AnnotationSetFilterField

  fieldLens AnnotationSetFilter_Set = annset

  tabulateFieldsA f = AnnotationSetFilter <$> f AnnotationSetFilter_Set

type AnnotationSettings = (AnnotationSetFilter, PatternFilter)
