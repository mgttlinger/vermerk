{-# LANGUAGE GADTs, TemplateHaskell, KindSignatures, FlexibleInstances, FlexibleContexts, MultiParamTypeClasses, TypeFamilies #-}

module Common.General.Api where

import Common.Auth
import Common.Pattern

import Control.Lens

import Data.Aeson
import Data.Aeson.GADT.TH
import Data.Constraint.Extras.TH
import Data.Map
import Data.Text

import Rhyolite.Api

data PublicApi :: * -> * where
  CheckLogin :: Login -> PublicApi (Maybe Text)

$(deriveJSONGADT ''PublicApi)
$(deriveArgDict ''PublicApi)
$(makePrisms ''PublicApi)

data PrivateApi :: * -> * where
  GetPatterns :: [PatternId] -> PrivateApi (Map PatternId Pattern)
  AllPatterns :: PrivateApi (Map PatternId Pattern)
  PatternCategories :: PrivateApi (Map Text [PatternId])
  PatternHierarchy :: PrivateApi [(PatternId, PatternId)]

$(deriveJSONGADT ''PrivateApi)
$(deriveArgDict ''PrivateApi)
$(makePrisms ''PrivateApi)

type GeneralApi = ApiRequest Login PublicApi PrivateApi
