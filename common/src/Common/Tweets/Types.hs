{-# LANGUAGE TemplateHaskell, DerivingStrategies, GeneralizedNewtypeDeriving, DeriveGeneric #-}

module Common.Tweets.Types where

import Common.Helpers

import Control.Lens

import Data.Aeson
import Data.Aeson.TH
import Data.Int
import Data.Text
import Data.Vector

import GHC.Generics

data Tweet = Tweet { _author :: Maybe Int64 -- ^ reference to the author of the tweet
                   , _screen_name :: Maybe Text -- ^ name of the tweet author at the time of writing
                   , _text :: Text -- ^ tweet content
                   , _text_normalized :: Maybe Text -- ^ pre processed tweet content
                   , _tokens :: Vector Text -- ^ tokenized tweet content
                   } deriving (Show, Eq, Generic)

newtype TweetId = TweetId Int64 deriving newtype (Eq, Show, Ord, FromJSON, ToJSON, ToJSONKey, FromJSONKey)

$(makeLenses 'TweetId)
$(makeLenses 'Tweet)
$(deriveJSON shortOptions ''Tweet)

tweet_text :: Lens' Tweet Text
tweet_text = text
