{-# LANGUAGE LambdaCase, TemplateHaskell, GADTs, KindSignatures, FlexibleInstances, MultiParamTypeClasses, TypeFamilies #-}

module Common.Tweets.Api where

import Common.Pattern
import Common.Annotation
import Common.Tweets.Types
import Common.Confusion
import Common.AnnotationSet

import Data.Text

import Data.Aeson hiding (Result)
import Data.Constraint.Extras.TH
import Data.Map (Map)
import Data.Int
import Data.Some

import Control.Lens hiding ((.=))

data PrivateTweetApi :: * -> * where
  GetTweets :: [TweetId] -> PrivateTweetApi (Map TweetId Tweet)
  AllTweets :: PrivateTweetApi (Map TweetId Tweet)
  RetrieveDisagreeing :: PrivateTweetApi (Map TweetId Tweet)
  AnnotationSets :: PrivateTweetApi [AnnotationSetId]
  Adjudicate :: TweetId -> PatternId -> AnnotationState -> Maybe Bool -> Bool -> PrivateTweetApi ()
  Annotate :: TweetId -> PatternId -> AnnotationState -> PrivateTweetApi ()
  UnAnnotate :: TweetId -> PatternId -> PrivateTweetApi ()
  ManualAnnotations :: PrivateTweetApi (Map TweetId (Map Text (Map PatternId [Annotation])))
  DisagreeingManualAnnotations :: PrivateTweetApi (Map TweetId (Map Text (Map PatternId [Annotation])))
  MyAnnotations :: PrivateTweetApi (Map TweetId (Map PatternId [AnnotationState]))
  GoldAnnotations :: PrivateTweetApi (Map TweetId (Map PatternId [GoldAnnotation]))
  NeedAdjudication :: PrivateTweetApi [TweetId]
  HasTopLevel :: PrivateTweetApi [TweetId]
  Confusions :: PrivateTweetApi (Map PatternId (Map (Text, Text) (ConfMatrix Int64)))
  GoldConfusions :: PrivateTweetApi (Map PatternId (Map Text (ConfMatrix Int64)))
  AnnotatedIn :: PatternId -> PrivateTweetApi (Map TweetId Tweet)
  SlotAnnotations :: PatternId -> PrivateTweetApi (Map TweetId (Map Int32 SlotAnnotation))
  NewSlotAnnotation :: TweetId -> PatternId -> SlotAnnotation -> PrivateTweetApi ()
  ModifySlotAnnotation :: TweetId -> PatternId -> Int32 -> Maybe SlotAnnotation -> PrivateTweetApi ()
  ModifyGoldAnnotation :: TweetId -> PatternId -> Maybe PatternId -> PrivateTweetApi ()

-- deriveJSONGADT raises errors calling for undecidable instances due to the pantom nature of the GADT type arguments so writing my own
instance ToJSON (PrivateTweetApi a) where
  toJSON = \case
    GetTweets tids -> toJSON ("tinfo", toJSON tids)
    AllTweets -> toJSON ("tweets", ())
    RetrieveDisagreeing -> toJSON ("disTweets", ())
    AnnotationSets -> toJSON ("sets", ())
    Adjudicate tid pid st tl undec -> toJSON ("adjud", toJSON (tid, pid, st, tl, undec))
    Annotate tid pid st -> toJSON ("annot", toJSON (tid, pid, st))
    UnAnnotate tid pid -> toJSON ("unannot", toJSON (tid, pid))
    ManualAnnotations -> toJSON ("manual", ())
    DisagreeingManualAnnotations -> toJSON ("disAnnots", ())
    MyAnnotations -> toJSON ("myAnnots", ())
    GoldAnnotations -> toJSON ("goldAnnots", ())
    NeedAdjudication -> toJSON ("needsAdjud", ())
    HasTopLevel -> toJSON ("hasTL", ())
    Confusions -> toJSON ("confs", ())
    GoldConfusions -> toJSON ("goldConfs", ())
    AnnotatedIn pid -> toJSON ("annotIn", toJSON pid)
    SlotAnnotations pid -> toJSON ("slots", toJSON pid)
    NewSlotAnnotation tid pid sa -> toJSON ("annotSlot", toJSON (tid, pid, sa))
    ModifySlotAnnotation tid pid s sa -> toJSON ("modSlot", toJSON (tid, pid, s, sa))
    ModifyGoldAnnotation tid pid pi -> toJSON ("modGold", toJSON (tid, pid, pi))

instance FromJSON (Some PrivateTweetApi) where
  parseJSON v = do
    (tag, rest) <- parseJSON v
    case tag of
      "tinfo" -> mkSome . GetTweets <$> parseJSON rest
      "tweets" -> pure $ mkSome AllTweets
      "disTweets" -> pure $ mkSome RetrieveDisagreeing
      "sets" -> pure $ mkSome AnnotationSets
      "adjud" -> do
        (tid, pid, st, tl, undec) <- parseJSON rest
        pure $ mkSome $ Adjudicate tid pid st tl undec
      "annot" -> do
        (tid, pid, st) <- parseJSON rest
        pure $ mkSome $ Annotate tid pid st
      "unannot" -> mkSome . uncurry UnAnnotate <$> parseJSON rest
      "manual" -> pure $ mkSome ManualAnnotations
      "disAnnots" -> pure $ mkSome DisagreeingManualAnnotations
      "myAnnots" -> pure $ mkSome MyAnnotations
      "goldAnnots" -> pure $ mkSome GoldAnnotations
      "needsAdjud" -> pure $ mkSome NeedAdjudication
      "hasTL" -> pure $ mkSome HasTopLevel
      "confs" -> pure $ mkSome Confusions
      "goldConfs" -> pure $ mkSome GoldConfusions
      "annotIn" -> mkSome . AnnotatedIn <$> parseJSON rest
      "slots" -> mkSome . SlotAnnotations <$> parseJSON rest
      "annotSlot" -> do
        (tid, pid, sa) <- parseJSON rest
        pure $ mkSome $ NewSlotAnnotation tid pid sa
      "modSlot" -> do
        (tid, pid, s, sa) <- parseJSON rest
        pure $ mkSome $ ModifySlotAnnotation tid pid s sa
      "modGold" -> do
        (tid, pid, pi) <- parseJSON rest
        pure $ mkSome $ ModifyGoldAnnotation tid pid pi


$(deriveArgDict ''PrivateTweetApi)
$(makePrisms ''PrivateTweetApi)
