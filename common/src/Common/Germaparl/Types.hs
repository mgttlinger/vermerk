{-# LANGUAGE TemplateHaskell, GeneralisedNewtypeDeriving, DerivingStrategies, DeriveGeneric #-}

module Common.Germaparl.Types where

import Common.Helpers

import Control.Lens

import Data.Aeson
import Data.Aeson.TH
import Data.Int
import Data.Text
import Data.Vector

import GHC.Generics

data Sentence = Sentence { _text :: Text
                         , _tokens :: Vector Text
                         } deriving (Show, Eq, Generic)

$(makeLenses ''Sentence)
$(makePrisms ''Sentence)
$(deriveJSON shortOptions ''Sentence)

newtype SentenceId = SentenceId Int64 deriving newtype (Eq, Show, Ord, FromJSON, ToJSON, ToJSONKey, FromJSONKey)

$(makeLenses 'SentenceId)
