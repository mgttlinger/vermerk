{-# LANGUAGE GADTs, TemplateHaskell, KindSignatures, FlexibleInstances, FlexibleContexts, MultiParamTypeClasses, TypeFamilies #-}

module Common.Germaparl.Api where

import Common.Germaparl.Types

import Control.Lens

import Data.Aeson
import Data.Aeson.GADT.TH
import Data.Constraint.Extras.TH
import Data.Map

data PrivateGermaApi :: * -> * where
  GetSentences :: [SentenceId] -> PrivateGermaApi (Map SentenceId Sentence)
  AllSentences :: PrivateGermaApi (Map SentenceId Sentence)

$(deriveJSONGADT ''PrivateGermaApi)
$(deriveArgDict ''PrivateGermaApi)
$(makePrisms ''PrivateGermaApi)
