{-# LANGUAGE TemplateHaskell, DeriveFunctor, FlexibleInstances #-}

module Common.Confusion where

import Control.Lens

import Data.Aeson
import Data.Aeson.TH
import Data.Monoid
import Data.Int

-- | Confusion Matrix for determining inter annotator agreement
data ConfMatrix c = ConfMatrix { _tt :: !c -- ^ both annotated true
                               , _ff :: !c -- ^ both annotated false
                               , _tf :: !c -- ^ first annotated true second annotated false
                               , _ft :: !c -- ^ second annotated false second annotated true
                               } deriving (Eq, Show, Functor)

$(makeLenses 'ConfMatrix)
$(deriveJSON defaultOptions 'ConfMatrix)

-- | Calculates Cohens Kappa score based on a confusion matrix
kappa :: (Fractional n, Real m) => ConfMatrix m -> n
kappa cm = let
  tp = cm ^. tt
  fp = cm ^. ft
  tn = cm ^. ff
  fn = cm ^. tf
  in realToFrac (2 * ((tp * tn) - (fn * fp))) / realToFrac (((tp + fp) * (fp + tn)) + ((tp + fn) * (fn + tn)))

-- | Calculates precision based on a confusion matrix
precision :: (Fractional n, Real m) => ConfMatrix m -> n
precision cm = let
  tp = cm^.tt
  fp = cm^.ft
  in realToFrac tp / realToFrac (tp + fp)

-- | Calculates precision based on a confusion matrix
recall :: (Fractional n, Real m) => ConfMatrix m -> n
recall cm = let
  tp = cm^.tt
  fn = cm^.tf
  in realToFrac tp / realToFrac (tp + fn)

-- | Count of positive annotations for both annotators
annotSizes :: Num n => ConfMatrix n -> (n,n)
annotSizes cm = (cm^.tt + cm^.tf, cm^.tt + cm^.ft)

-- | Semigroup for a confusion matrix works pointwise deferring to the smaller semigroup
instance Semigroup a => Semigroup (ConfMatrix a) where
  cma <> cmb =
    ConfMatrix
    (cma^.tt <> cmb^.tt)
    (cma^.ff <> cmb^.ff)
    (cma^.tf <> cmb^.tf)
    (cma^.ft <> cmb^.ft)

-- | Monoid for a confusion matrix works pointwise deferring to the smaller monoid
instance Monoid a => Monoid (ConfMatrix a) where
  mempty = ConfMatrix mempty mempty mempty mempty

-- | Construct a confusion matrix based on two annotators
confuse :: Monoid b => (a -> b) -> (a -> Bool) -> (a -> Bool) -> [a] -> ConfMatrix b
confuse collect annotation1 annotation2 = foldl accumulate mempty
  where
    field av = case (annotation1 av, annotation2 av) of
      (True, True) -> tt
      (False, False) -> ff
      (True, False) -> tf
      (False, True) -> ft
    accumulate accu av = accu & field av %~ (collect av <>)

-- | Construct a confusion matrix that only counts the occurrences
confuseCount :: (a -> Bool) -> (a -> Bool) -> [a] -> ConfMatrix Int64
confuseCount a1 a2 = fmap (getSum) . confuse (\ _ -> Sum 1) a1 a2

-- | Construct a confusion matrix that collects the annotated candidates
confuseCollect :: (a -> Bool) -> (a -> Bool) -> [a] -> ConfMatrix [a]
confuseCollect = confuse (\ a -> [a])
