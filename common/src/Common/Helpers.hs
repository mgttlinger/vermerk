{-# LANGUAGE TypeApplications, ScopedTypeVariables #-}

module Common.Helpers where

import Control.Applicative
import Control.Monad
import Control.Lens
import qualified Control.Monad.Fail as F

import qualified Data.Text as T
import Data.Maybe
import Data.List
import Data.Map
import Data.Monoid
import Data.Coerce
import Data.Aeson hiding (Result)
import Data.Aeson.TH
import qualified Data.Aeson.Types as A

import Text.Trifecta


-- | Test if a list only contains unique elements
unique :: Eq a => [a] -> Bool
unique [] = True
unique (x:xs) = x `notElem` xs && unique xs

-- | Unordered non reflexive pairs
upairs :: Eq a => [a] -> [(a,a)]
upairs [] = []
upairs (x:xs) = [(x,y) | y <- xs] <> upairs xs

-- | Calculates the length of the prefix necessary that the list stays unique
uniquePrefixLength :: [T.Text] -> Int
uniquePrefixLength texts = fromMaybe 0 $ find (\ l -> unique $ T.take l <$> texts) [1..]

-- | Conditionally applies an endomorphism
appWhen :: (a -> a) -> Bool -> a -> a
appWhen f True = f
appWhen _ False = id

-- | Lifted version of 'appWhen'
appWhenM :: Applicative m => m (a -> a) -> m Bool -> m a -> m a
appWhenM = liftA3 appWhen

-- | Converts an indexed container to a map from index to value
toIxMap :: (FoldableWithIndex i f, Ord i) => f a -> Map i a
toIxMap = ifoldr' (\ i a accu -> accu & at i .~ Just a) Data.Map.empty

-- from https://stackoverflow.com/a/51145120/1876344
safeToEnum :: (Enum a, Bounded a, F.MonadFail m) => Int -> m a
safeToEnum i = if i < minV
               then F.fail $ show i ++ " is less than the minimum, " ++ show minV
               else if i > maxV
                    then F.fail $ show i ++ " is greater than the maximum, " ++ show maxV
                    else return result
  where minV = fromEnum (minBound `asTypeOf` result)
        maxV = fromEnum (maxBound `asTypeOf` result)
        result = toEnum i -- hooray laziness

resultEither :: Result a -> Either String a
resultEither = foldResult (Left . show) Right

recM :: (Monoid a) => Either b a -> a
recM (Left _) = mempty
recM (Right a) = a

parseText :: Parser t -> T.Text -> Either String t
parseText p t = resultEither $ parseString p mempty $ T.unpack t

takingEdge :: Eq b => Getting (Endo [b]) a b -> [a] -> [a]
takingEdge _ [] = []
takingEdge f (a : rest) = case a ^.. f of
                            x : _ -> a : takingEdge' x rest -- take until the first value is different from a
                            [] -> a : takingEdge f rest -- ignore leading non-values
  where
    takingEdge' _ [] = []
    takingEdge' s (b : rst) = if all (s ==) (b ^.. f) then b : takingEdge' s rst else [b]

shortOptions = defaultOptions {fieldLabelModifier = (\ l -> Prelude.drop (Prelude.length l - 2) l), omitNothingFields = True}

deriveShortJSON = deriveJSON shortOptions

newtype ViaEnum t = ViaEnum t

instance (Enum t, Bounded t) => FromJSON (ViaEnum t) where
  parseJSON = coerce @(A.Parser t) @(A.Parser (ViaEnum t)) . safeToEnum <=< parseJSON

instance Enum t => ToJSON (ViaEnum t) where
  toJSON = toJSON . fromEnum . coerce @(ViaEnum t) @(t)


collectIntoMap :: (Ord c, Monoid (f b)) => (a -> c) -- ^ calculates the key to store at
               -> (a -> f b) -- ^ calculates the value to accumulate
               -> [a] -> Map c (f b)
collectIntoMap key val = fromListWith (<>) . fmap (\ av -> (key av, val av))

toMap :: (Ord k) => (a -> v) -> [(k, a)] -> Map k v
toMap c vs = fromList [(k, c a) | (k, a) <- vs]
