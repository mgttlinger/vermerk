{-# LANGUAGE TemplateHaskell, FlexibleInstances, FlexibleContexts, MultiParamTypeClasses, TypeFamilies, UndecidableInstances #-}

module Common.Apis where

import Common.Auth
import Common.Settings
import Common.General.Api
import Common.Tweets.Api
import Common.Germaparl.Api

import Control.Lens
import Control.Monad.Reader

import Data.Aeson
import Data.Some
import Data.Constraint.Extras
import Data.Coerce

import Rhyolite.Api

data Apis a = General AnnotationSettings (GeneralApi a)
            | Tweet AnnotationSettings Login (PrivateTweetApi a)
            | Germaparl AnnotationSettings Login (PrivateGermaApi a)

$(makePrisms ''Apis)

instance ToJSON (Apis a) where
  toJSON (General s g) = toJSON ("gen", (toJSON s, toJSON g))
  toJSON (Tweet s l g) = toJSON ("t", (toJSON s, toJSON l, toJSON g))
  toJSON (Germaparl s l g) = toJSON ("g", (toJSON s, toJSON l, toJSON g))

instance FromJSON (Some Apis) where
  parseJSON v = do
    (tag, rest) <- parseJSON v
    case tag of
      "gen" -> do
        (s, g) <- parseJSON rest
        mapSome (General s) <$> parseJSON g
      "t" -> do
        (s, l, sr) <- parseJSON rest
        return $ mapSome (Tweet s l) sr
      "g" -> do
        (s, l, sr) <- parseJSON rest
        return $ mapSome (Germaparl s l) sr

-- Raises exception... $(deriveArgDict ''Apis)
-- so I'm instantiating on my own
instance ArgDict c Apis where
  type ConstraintsFor Apis c = (ConstraintsFor GeneralApi c, ConstraintsFor PrivateTweetApi c, ConstraintsFor PrivateGermaApi c)
  argDict (General _ x) = argDict x
  argDict (Tweet _ _ x) = argDict x
  argDict (Germaparl _ _ x) = argDict x

newtype Deferred a = Deferred (ReaderT (AnnotationSettings, Login) Apis a)

gen :: PrivateApi a -> Deferred a
gen r = coerce $ \ (settings, login) -> General settings $ private login r

tw :: PrivateTweetApi a -> Deferred a
tw r = coerce $ \ (settings, login) -> Tweet settings login r

ger :: PrivateGermaApi a -> Deferred a
ger r = coerce $ \ (settings, login) -> Germaparl settings login r

-- $ Empty the annotation filter in the settings
withAllPatterns :: Deferred a -> Deferred a
withAllPatterns (Deferred c) = Deferred $ withReaderT (set' (_1 . _2 . patterns) []) c

logIn :: Login -> Deferred a -> ReaderT AnnotationSettings Apis a
logIn lin = withReaderT (\ s -> (s, lin)) . coerce
