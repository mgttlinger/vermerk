# Vermerk
## Running locally
Using the obsidiansystems/obelisk tooling this app can be run/develloped locally using `ob run`
## Deployment
This app uses obelisk so it can be deployed as Android or iOS app as well as on EC2 using the obsidiansystems/obelisk tooling.

Alternatively you can deploy it using nix to build a docker image via `nix-build docker.nix` that can then be loaded into docker as `vermerk:latest` via `docker load -i result`
