{-# LANGUAGE LambdaCase, RecursiveDo, OverloadedStrings, FlexibleContexts #-}

module Pages where

import Reflex
import Reflex.Dom

import Common.Apis
import Common.Pattern
import Common.Annotation
import Common.Helpers
import Common.Confusion
import Common.Settings
import Common.Tweets.Api
import Common.Tweets.Types hiding (text)
import qualified Common.Tweets.Types as Tweets

import qualified Data.Map as Map
import qualified Data.Text as T

import Data.Maybe
import Data.Monoid
import Data.Int
import Data.List
import Data.Text.Lens hiding (text)

import Control.Monad
import Control.Monad.Fix
import Control.Monad.IO.Class
import Control.Lens


import GeneralWidgets
import Components
import TweetWidgets

adjudicationPage :: (DomBuilder t m, MonadHold t m, MonadFix m, PostBuild t m, Prerender t m, MonadIO (Performable m), TriggerEvent t m, PerformEvent t m) => VermerkWidget t m ()
adjudicationPage = do
  tweetSet <- load $ tw RetrieveDisagreeing
  annotsLookup <- loadAndRefresh 2 $ tw DisagreeingManualAnnotations
  needsAdjud <- loadAndRefresh 1 $ tw NeedAdjudication
  gold <- loadAndRefresh 2 $ tw GoldAnnotations
  hasTop <- loadAndRefresh 2 $ tw HasTopLevel
  let annotators = nub . (Map.keys <=< Map.elems) <$> annotsLookup
  let goldId = "gold"
  let uPrefixLen =  (\ anns -> uniquePrefixLength (goldId : anns)) <$> annotators
  let goldName = T.take <$> uPrefixLen <*> pure goldId
  let annotatorsP = (\ ulen -> map (T.take ulen)) <$> uPrefixLen <*> annotators
  let trimNames m = (\ ulen -> fmap (Map.mapKeys (T.take ulen))) <$> uPrefixLen <*> m
  let annLookupP = trimNames annotsLookup
  let needsAdjudication tid = (tid `elem`) <$> needsAdjud
  let hasTopLevel tid = (tid `elem`) <$> hasTop
  let tweetAttribute tid = ffor (needsAdjudication tid) $ \case
                                                             True -> mempty & at "class" .~ Just "needsAdjudication"
                                                             False -> mempty
  let adjudFilter = filterAdjudicated <$> needsAdjud
  rec _ <- paginated 10 (filters annLookupP) $ \ alp -> do
        pattsFromSettings <- fmap _patterns <$> askPatternFilter
        let subGold = do
              a <- alp
              Map.filterWithKey (\ tid _ -> tid `elem` (a ^.. itraversed . asIndex)) <$> gold
        let annotPatts = do
              a <- alp
              g <- subGold
              return $ nub $ (g ^.. traversed . itraversed . asIndex) <> (a ^.. traversed . traversed . itraversed . asIndex)
        let fpatts = do
              s <- pattsFromSettings
              a <- annotPatts
              return $ union s a
        fpattInfo <- getPatternInfo fpatts
        tableDynAttr "adjudicationTable"
          [ ("Tweet", \ tid _ -> fullTweet tid (ffor tweetSet $ fromJust . view (at tid)) (pure False))
          , ("Annotations", \ tid annotPatLookup -> annotationListDisplay tid (filter <$> showAnnotator <*> annotatorsP) annotPatLookup fpattInfo goldName (ffor subGold (view $ ix tid)) showGold showAnnotator (hasTopLevel tid))
           ]
           alp
           (return . tweetAttribute)
      el "h3" $ text "Settings"
      (filterAdjud, showGold) <- divClass "row" $ do
        filterAdjud <- divClass "column" $ value <$> toggleSwitch (pure "filter adjudicated") True
        showGold <- divClass "column" $ value <$> toggleSwitch (pure "show gold") False
        return (filterAdjud, showGold)
      el "h4" $ text "Include annotator:"
      showAnnotator <- divClass "row" $ filterConcatM <$> (simpleList annotators $ \ dannor -> divClass "column" $ do
                                                              dv <- value <$> toggleSwitch dannor False

                                                              return $ do
                                                                aor <- dannor
                                                                v <- dv
                                                                return $ if v
                                                                         then \ s -> T.isPrefixOf s aor
                                                                         else const False)
      let filters = appEndo $
                    mconcat $
                    Endo . uncurry appWhenM <$> [ (adjudFilter, filterAdjud)
                                                ]


  return ()
  where
    filterConcatM :: (Monad m, Traversable f) => m (f (m (a -> Bool))) -> m (a -> Bool)
    filterConcatM dldf = do
      lf <- join $ sequence <$> dldf
      return $ \ av -> any ($ av) lf

annotatePage :: (DomBuilder t m, MonadFix m, MonadHold t m, PostBuild t m) => Map.Map TweetId Tweet -> Dynamic t (Map.Map PatternId Pattern) -> Dynamic t (Map.Map T.Text [PatternId]) ->  Dynamic t (Map.Map TweetId (Map.Map PatternId [AnnotationState])) -> VermerkWidget t m ()
annotatePage tweets patts patternCats annots = do
  el "div" $ do
    let keyEvs = never
    mselTweet <- fmap (fmap fst) <$> tweetSelection keyEvs tweets
    patternToggleMenu patts patternCats mselTweet (do
                                                         ans <- annots
                                                         mtweet <- mselTweet
                                                         return $ case mtweet of
                                                           Just sel -> ans ^. ix sel
                                                           Nothing -> mempty
                                                     )
  return ()

agreementPage :: (DomBuilder t m, MonadHold t m, MonadFix m, PostBuild t m, Prerender t m) => VermerkWidget t m ()
agreementPage = do
  confusions <- loadAndRefresh 60 $ tw Confusions
  goldConfusions <- loadAndRefresh 30 $ tw GoldConfusions
  let patts = do
        c1 <- confusions
        c2 <- goldConfusions
        return $ nub $ (c1 ^.. ifolded . asIndex) <> (c2 ^.. ifolded . asIndex)
  paginated 5 (Map.fromList . fmap (\ p -> (p,p)) <$> patts) $ flip listWithKey $ \ pid _ -> do
    labelled (show pid ^. packed) $ do
      labelled "scores" $ do
        listWithKey ((view $ ix pid) <$> goldConfusions) $ \ a dcmat ->
          labelled ("gold/" <> a) $ renderScores dcmat
        listWithKey ((view $ ix pid) <$> confusions) $ \ anames dcmat ->
          labelled (anames^._1 <> "/" <> anames^._2) $ renderScores dcmat
  return ()

renderScores :: (DomBuilder t m, PostBuild t m) => Dynamic t (ConfMatrix Int64) -> m ()
renderScores dcmat =
  let
    tshow5 v = if abs v < 0.0
               then "~0.0"
               else T.pack $ take 5 $ show v
    s f c = tshow5 (f c)
  in do
    dynText $ (\ c -> " k " <> s kappa c <> " / p " <> s precision c <> " / r " <> s recall c <> " / #" <> T.pack (show (annotSizes c))) <$> dcmat

filterAnyPosAnnotated :: (Eq p) => [p] -> Map.Map t (Map.Map a (Map.Map p [Annotation])) -> Map.Map t (Map.Map a (Map.Map p [Annotation]))
filterAnyPosAnnotated pats = Map.filter (anyOf (folded . ifolded . ifiltered (\ k _ -> k `elem` pats) . folded . annotationState . annStateBool) id)

filterAdjudicated :: (Eq t) => [t] -> Map.Map t v -> Map.Map t v
filterAdjudicated notAdjud = Map.filterWithKey $ \ k _ -> k `elem` notAdjud

annotatorFilter :: (a -> Bool) -> Map.Map t (Map.Map a v) -> Map.Map t (Map.Map a v)
annotatorFilter f = fmap (Map.filterWithKey (const . f))
