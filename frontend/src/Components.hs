{-# LANGUAGE RecursiveDo, LambdaCase, FlexibleContexts, OverloadedStrings, GADTs #-}

module Components where


import Prelude hiding (tail, filter)

import Control.Monad

import Reflex.Network
import Reflex.Dom.Core

import Obelisk.Route.Frontend

import Control.Monad.Identity
import Control.Monad.IO.Class
import Control.Lens

import Data.Text (Text)
import Data.Text.Lens hiding (text)
import Data.Either
import Data.Foldable
import Data.Functor
import Data.Maybe hiding (mapMaybe)
import Data.Time.Clock
import Data.Functor.Misc
import qualified Data.Map as Map
import qualified Data.List as List
import qualified Data.List.NonEmpty as NList

-- | Applies a function in an event by sampling a dynamic of the input value
sampleApp :: (Reflex t) => Event t (a -> b) -> Dynamic t a -> Event t b
sampleApp ef da = attachPromptlyDynWith (&) da ef

-- | Applies a function in an event by sampling a dynamic of the input value
sampleMApp :: (Reflex t) => Event t (a -> b) -> Dynamic t (Maybe a) -> Event t b
sampleMApp ef da = fmapMaybe id $ attachPromptlyDynWith (ffor) da ef

-- | Displays event producing UI components depending on the current value of a dyanmic value
dynNetworkE :: (MonadHold t m, NotReady t m, Adjustable t m, PostBuild t m) => Dynamic t a -> (a -> m (Event t b)) -> m (Event t b)
dynNetworkE da = switchHold never <=< networkView . ffor da

dynNetwork :: (NotReady t m, Adjustable t m, PostBuild t m) => Dynamic t a -> (a -> m ()) -> m ()
dynNetwork db w = do
  _ <- networkView $ ffor db $ w
  blank

whenNetworkE :: (MonadHold t m, NotReady t m, Adjustable t m, PostBuild t m) => Dynamic t Bool -> m (Event t b) -> m (Event t b)
whenNetworkE db w = dynNetworkE db $ \ b -> if b then w else return never

whenNetwork :: (NotReady t m, Adjustable t m, PostBuild t m) => Dynamic t Bool -> m () -> m ()
whenNetwork db w = do
  _ <- networkView $ ffor db $ \ b -> if b then w else return ()
  blank

whenMNetwork :: (MonadHold t m, NotReady t m, Adjustable t m, PostBuild t m) => Dynamic t (Maybe a) -> (a -> m b) -> m ()
whenMNetwork dma w = do
  _ <- networkView $ ffor dma $ \case
    Nothing -> return ()
    Just a -> w a $> ()
  blank

-- | Displays dynamic producing UI componenents depending on the current value of a dynamic value
dynNetworkD :: (MonadHold t m, Adjustable t m) => Dynamic t a -> (a -> m (Dynamic t b)) -> m (Dynamic t b)
dynNetworkD da f = do
  av <- sample $ current da
  ddb <- networkHold (f av) (f <$> updated da)
  return $ join ddb

-- | Hold the 'Right' value of an event defaulting to 'mempty' on error and initially
holdDynMonoid :: (Monoid b, MonadHold t m, Reflex t) => Event t (Either a b) -> m (Dynamic t b)
holdDynMonoid = holdDyn mempty . fmap (fromRight mempty)

-- | Runtime deferred version of 'requesting'
requestingJS :: (MonadFix m, Prerender t m, Requester t (Client m)) => (Client m) (Event t (Request (Client m) a)) -> m (Event t (Response (Client m) a))
requestingJS mr = switchDyn <$> prerender (pure never) (requesting =<< mr)

getOn :: (MonadFix m, Prerender t m, Requester t (Client m)) => [(Client m) (Event t ())] -> (Request (Client m) a) -> m (Event t (Response (Client m) a))
getOn evs r = requestingJS $ leftmost . fmap (\ e -> r <$ e) <$> sequence evs

-- | Trigger request post build
getOnLoaded :: (MonadFix m, Prerender t m, Requester t (Client m)) => (Request (Client m) a) -> m (Event t (Response (Client m) a))
getOnLoaded r = getOn [getPostBuild] r

-- | Periodically refreshing variant of 'getOnLoaded'
getAndRefresh :: (MonadFix m, Prerender t m, Requester t (Client m)) => NominalDiffTime -> (Request (Client m) a) -> m (Event t (Response (Client m) a))
getAndRefresh interv r = switchDyn <$> (prerender (pure never) $ do
  rec grd <- current <$> toggle True (leftmost [() <$ res, () <$ pb, () <$ tick])
      tick <- gate grd . (r <$) <$> tickLossyFromPostBuildTime interv
      pb <- gate grd . (r <$) <$> getPostBuild
      res <- requesting $ leftmost [pb, tick]
  return res)

-- | Request data from the backend post build recovering with 'mempty'
load :: (Monoid a, MonadFix m, Prerender t m, MonadHold t m, Requester t (Client m), Response (Client m) ~ Either a0) => (Request (Client m) a) -> m (Dynamic t a)
load = holdDynMonoid <=< getOnLoaded

-- | Periodically refreshing variant of 'load'
loadAndRefresh :: (Eq a, Monoid a, MonadFix m, Prerender t m, MonadHold t m, Requester t (Client m), Response (Client m) ~ Either a0) => NominalDiffTime -> (Request (Client m) a) -> m (Dynamic t a)
loadAndRefresh interv r = holdUniqDyn =<< holdDynMonoid =<< getAndRefresh interv r

-- | Variant of 'load' using an external event as trigger
loadOn :: (Monoid a, MonadFix m, Prerender t m, MonadHold t m, Requester t (Client m), Response (Client m) ~ Either a0) => Event t (Request (Client m) a) -> m (Dynamic t a)
loadOn = holdDynMonoid <=< requestingJS . pure

-- | Adds an HTML label element to the given widget
labelled :: (DomBuilder t m) => Text -> m a -> m a
labelled label = el "label" . (text label >>)

-- | Adds an HTML label element to the given widget
dlabelled :: (DomBuilder t m, PostBuild t m) => Dynamic t Text -> m a -> m a
dlabelled label = el "label" . (dynText label >>)

toggleSwitch :: (DomBuilder t m, PostBuild t m) => Dynamic t Text -> Bool -> m (Checkbox t)
toggleSwitch label start = el "label" $ dynText label *> el "br" blank *> checkbox start (def {_checkboxConfig_attributes = pure ("class" =: "toggle")})

-- | Widget for editable text value
entrySet :: (DomBuilder t m, MonadFix m, MonadHold t m, PostBuild t m) => Text -> m (Dynamic t (Maybe Text))
entrySet label = do
  rec e <- dynNetworkE de $ \case
        Nothing -> divClass "row" $ do
          tb <- labelled label $ inputElement def
          but <- button "Done"
          return $ Just <$> tag (current $ value tb) (leftmost [but, keydown Enter tb])
        Just a -> do
          (ie,_) <- el' "span" $ text a
          return $ Nothing <$ domEvent Click ie
      de <- holdDyn Nothing e
  return de

-- | Button that produces a value rather than an unit event
contentButton :: (DomBuilder t m) => a -> (a -> Text) -> m (Event t a)
contentButton av p = do
  e <- button $ p av
  pure $ av <$ e

staticIdMapList :: (Ord ia, Applicative m, Reflex t, Adjustable t m, PostBuild t m, MonadHold t m, MonadFix m) => Map.Map ia a -> Dynamic t ia -> (ia -> Dynamic t a -> Dynamic t Bool -> m (Event t e)) -> m (Event t [e])
staticIdMapList els sel widget = fmap Map.elems <$> listViewWithKey (constDyn els) (\ i e -> widget i e ((i ==) <$> sel))

data SelectListAction i = Sel i
                        | Next
                        | Prev

selectListAction :: (Ord i, Foldable l) => l i -> Maybe i -> SelectListAction i -> Maybe i
selectListAction idas _ (Sel i) | i `elem` idas = Just i
                                | otherwise = Nothing
selectListAction idas oi Next = case oi of
  Nothing -> listToMaybe $ toList idas
  Just oi -> List.find (> oi) idas
selectListAction idas oi Prev = case oi of
  Nothing -> listToMaybe $ toList idas
  Just oi -> List.find (< oi) $ reverse $ toList idas

selectList :: (Show ida, Ord ida, Applicative m, DomBuilder t m, PostBuild t m, MonadHold t m, MonadFix m) => Map.Map ida a -> (ida -> Dynamic t a -> Dynamic t Bool -> m (Event t (SelectListAction ida), Event t b)) -> Event t (SelectListAction ida) -> m (Dynamic t (Maybe (ida, a)), Event t (NList.NonEmpty b))
selectList els widget externalEvent = let keys = Map.keys els in
   do
    rec mselected <- accumMaybeDyn (fmap Just . selectListAction keys) Nothing (leftmost $ externalEvent : evs)
        (evs, evsPass) <- el "ul" $
          List.unzip . fmap fanEither . (<$> (Const2 <$> keys)) . select . fanMap <$>
          listViewWithKey (constDyn els) (\ i e ->
                                            let sel = (maybe False (i ==) <$> mselected)
                                                attr = ffor sel $ (("id" =: (show i ^. packed)) <>) . \case
                                                  False -> mempty
                                                  True -> "class" =: "selected"
                                            in
                                            do
                                              (ie,(ev, evPassthrough)) <- elDynAttr' "li" attr $ widget i e sel
                                              let klickEv = Sel i <$ (gate (current $ not <$> sel) $ domEvent Click ie)
                                              return $ leftmost [Left <$> klickEv, Left <$> ev, Right <$> evPassthrough]
                                         )

    return $ (fmap (\ sel -> (sel, els Map.! sel)) <$> mselected, mergeList evsPass)

singleSelect :: (DomBuilder t m, Reflex t, Ord a, MonadHold t m, MonadFix m) => Dynamic t (NList.NonEmpty a) -> (Dynamic t a -> m ()) -> m (Dynamic t a)
singleSelect das render = dynNetworkD das $ \ as -> divClass "row" $ do
  prev <- button "<"
  rec selA <- accumMaybeDyn (selectListAction as . Just) (NList.head as) $ leftmost [Prev <$ prev, Next <$ next]
      render selA
      next <- button ">"
  return selA

-- | Dropdown selecting from a 'Map' of values
contentDropdown :: (DomBuilder t m, Ord k, MonadHold t m, MonadFix m, PostBuild t m) => Dynamic t (Map.Map k a) -> (k -> a -> Text) -> m (Dynamic t (Maybe (k, a)))
contentDropdown els shw =
  let
    keys = Map.keys <$> els
  in
  dynNetworkD keys $ \case
    [] -> return $ constDyn Nothing
    k1 : _ -> do
      sel <- value <$> dropdown k1 (Map.mapWithKey shw <$> els) def
      return $ ffor2 els sel $ \ elmap selection -> do
        r <- elmap Map.!? selection
        return (selection, r)


-- | Dropdown selecting from a 'Map' of values emmitting selection changes
simpleDropdown :: (DomBuilder t m, Ord a, MonadHold t m, MonadFix m, PostBuild t m) => Dynamic t (NList.NonEmpty a) -> (a -> Text) -> m (Dynamic t a)
simpleDropdown dels render =
  let
    keys = Map.fromList . NList.toList . fmap (\ a -> (a, render a)) <$> dels
  in
    do
      fe <- sample $ current dels
      value <$> dropdown (NList.head fe) keys def

-- | Displays a list of widgets producing events into a widget returning the first firing event
listEvent :: (DomBuilder t m, MonadHold t m, PostBuild t m, Ord k, MonadFix m) => Dynamic t (Map.Map k v) -> (k -> Dynamic t v -> m (Event t a)) -> m (Event t a)
listEvent dat widget =
  let keys = Map.keys <$> dat
  in
    dynNetworkE keys $ \case
      [] -> return never
      ks -> do
        eMapA <- listViewWithKey dat widget
        return $ leftmost $ (<$> (Const2 <$> ks)) $ select $ fanMap eMapA

-- * Menu/Submenu
--   A widget for layered menus where the user navigates through a hierarchy of menus and submenus

-- | Event payload of the menu
data MenuAction k a = Target a -- ^ Submenu target event to propagate
                    | Sub k -- ^ Enter submenu
                    | Up -- ^ Back to the parent menu
                    deriving (Eq, Ord, Show)

-- | State accumulation function of one menu layer
accumMenu :: Maybe k -> MenuAction k a -> Maybe k
accumMenu m (Target _) = m -- propagating target events doesn't affect the state
accumMenu _ (Sub k) = Just k -- Entering submenu selects the submenu
accumMenu _ Up = Nothing -- Going up forgets the selected submenu

-- | Widget to display one menu layer
menuLayer :: (MonadFix m, DomBuilder t m, MonadHold t m, PostBuild t m, Ord k) => Dynamic t (Map.Map k v) -- ^ Submenus
  -> (k -> Text) -- ^ Name of a submenu
  -> (k -> Dynamic t v -> m (Event t a)) -- ^ Widget for the submenu
  -> m (Event t a) -- ^ Propagated events from the submenus
menuLayer dat butt submenu = do
  rec selected <- accumDyn accumMenu Nothing sel
      sel <- dynNetworkE selected $ \case
        Nothing -> listEvent dat $ \ kv _ -> do
          subEv <- button $ butt kv
          return $ Sub kv <$ subEv
        Just kv -> do
          back <- button "<"
          subev <- submenu kv $ fromJust . Map.lookup kv <$> dat
          return $ leftmost [Up <$ back, Target <$> subev]
  return $ mapMaybe (\case
                        Target a -> Just a
                        _ -> Nothing
                    ) sel

yesNoUndecButton :: (DomBuilder t m, PostBuild t m) => Text -> (s -> Text) -> (s -> Text) -> (s -> Text) -> Dynamic t s -> m () -> m () -> m () -> m (Event t (Maybe Bool))
yesNoUndecButton cls classY classU classN state contentY contentU contentN = divClass ("ynu-button " <> cls) $ do
  (yes,_) <- elDynAttr' "span" (("class" =:) . classY <$> state) contentY
  (undec, _) <- elDynAttr' "span" (("class" =:) . classU <$> state) contentU
  (no, _) <- elDynAttr' "span" (("class" =:) . classN <$> state) contentN
  return $ leftmost [Just True <$ domEvent Click yes, Just False <$ domEvent Click no, Nothing <$ domEvent Click undec]

splitLength :: Int -> [a] -> [[a]]
splitLength _ [] = []
splitLength i l = let (s1, rest) = splitAt i l in s1 : splitLength i rest

paginationClass :: Bool -> Text
paginationClass True = "selected " <> paginationClass False
paginationClass False = "pageSelect"

paginated :: (DomBuilder t m, Ord k, MonadHold t m, MonadFix m, PostBuild t m) => Int -> Dynamic t (Map.Map k v) -> (Dynamic t (Map.Map k v) -> m r) -> m r
paginated size dat view =
  let datasize = Map.size <$> dat
      pagecount = ffor datasize $ \ ds -> let (d,m) = divMod ds size in d + signum m
      large = ffor pagecount (> 100)
      pages = map Map.fromAscList . splitLength size . Map.toAscList <$> dat
      page i = ffor pages $ \ ps -> if length ps >= i then ps !! (i - 1) else mempty
      pagenums = do
        l <- large
        Map.fromList . (\ mn -> (\ n -> (n, show n ^. packed)) <$> [1..mn]) <$> if l then pure 100 else pagecount
  in divClass "paginated" $ do
    rec curPageNum <- accumDyn (curry snd) 1 selPage
        let curPage = do
              cpn <- curPageNum
              page cpn
        res <- view curPage
        selPage <- divClass "fixed bottom" $ labelled "Page" $ do
          r <- selectViewListWithKey_ curPageNum pagenums $ \ _ txt sel -> do
            (e, _) <- elDynClass' "button" (paginationClass <$> sel) $ dynText txt
            return $ domEvent Click e
          whenNetwork large $ text "..."
          return r
    return res


loader :: (NotReady t m, PostBuild t m, Adjustable t m, DomBuilder t m) => Dynamic t (Maybe a) -> (a -> m b) -> m ()
loader dma view = do _ <- networkView $ ffor dma $ \case
                       Nothing -> text "Loading..." $> ()
                       Just a -> view a $> ()
                     blank

addWidget :: (DomBuilder t m, MonadHold t m, MonadFix m, PostBuild t m) => m (Dynamic t a) -> m (Event t a)
addWidget widget = do
  rec active <- toggle False e
      e <- dynNetworkE active $ \case
        False -> (Nothing <$) <$> button "Add"
        True -> do
          wv <- widget
          s <- button "Save"
          c <- button "Cancel"
          return $ leftmost [Nothing <$ c, Just <$> current wv <@ s]
  return $ fmapMaybe id e

popSelect :: (DomBuilder t m, Ord k, PostBuild t m, MonadHold t m, MonadFix m) => Dynamic t (Map.Map k Text) -> Dynamic t k -> m (Event t k)
popSelect content state = divClass "popup" $ do
  selectViewListWithKey_ state content $ \ k dv sel -> do
    (e, _) <- elClass' "span" "button" $ dynText dv
    return $ domEvent Click e

popToggle :: (DomBuilder t m, MonadFix m, PostBuild t m, MonadHold t m) => Bool -> Dynamic t Text -> m (Event t (), a) -> m a
popToggle initial addClasses widget = do
  rec displayed <- toggle initial tog
      (tog, res) <- elDynClass "div" (((\ b -> if b then "popup " else "popup hidden ") <$> displayed) <> addClasses) widget
  return res

nowVal :: (PostBuild t m, PerformEvent t m, TriggerEvent t m, MonadIO (Performable m)) => Dynamic t a -> m (Event t a)
nowVal da = do
  e <- tagPromptlyDyn da <$> getPostBuild
  delay 0.1 e

nowAndUpdated :: (PostBuild t m, PerformEvent t m, TriggerEvent t m, MonadIO (Performable m)) => Dynamic t a -> m (Event t a)
nowAndUpdated da = do
  c <- nowVal da
  return $ leftmost [updated da, c]


-- | Wrapper around an event that displays a waiting message as well as an error message in case of failure. If the event fires multiple times it might recover from the error or otherwise display the error message indefinitely
await :: (DomBuilder t m, MonadHold t m, Adjustable t m) => Text -> Event t (Either Text a) -> m (Event t a)
await name ev =
  let success = snd $ fanEither ev in
    do
      _ <- networkHold (text $ "waiting for " <> name) $ ffor ev $
        \case
          Left err -> text $ "while waiting for " <> name <> " got error: " <> err
          Right _ -> blank
      return success
