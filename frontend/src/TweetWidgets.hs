{-# LANGUAGE DataKinds, ConstraintKinds #-}
{-# LANGUAGE OverloadedStrings, LambdaCase #-}
{-# LANGUAGE TypeApplications, FlexibleContexts, GADTs, RankNTypes, RecursiveDo, ScopedTypeVariables, QuantifiedConstraints, PackageImports, PartialTypeSignatures, FlexibleInstances, MultiParamTypeClasses #-}


module TweetWidgets where

import Components
import GeneralWidgets

import Prelude hiding (tail)

import qualified Data.Text as T

import Reflex.Dom.Core hiding (preventDefault)
import Obelisk.Route.Frontend
import Rhyolite.Api

import Common.Apis
import Common.Route
import Common.Settings
import Common.Annotation
import Common.AnnotationSet
import Common.Pattern
import Common.Tweets.Api
import Common.Tweets.Types hiding (text)
import qualified Common.Tweets.Types as Tweets

import Control.Monad.Identity
import Control.Lens hiding (set, uncons)
import Control.Applicative
import qualified Data.Map as Map
import Data.Maybe
import Data.Text (Text)
import Data.Text.Lens hiding (text)

setSettings :: (SetRoute t (R FrontendRoute) m) => Event t AnnotationSettings -> m ()
setSettings s = modifyRoute $ ffor s $ \ st ->
  \case
    FrontendRoute_Main :/ () -> FrontendRoute_Main :/ ()
    FrontendRoute_Tweets :/ Annot :/ _ ->  FrontendRoute_Tweets :/ Annot :/ st
    FrontendRoute_Tweets :/ Adjud :/ _ -> FrontendRoute_Tweets :/ Adjud :/ st
    FrontendRoute_Tweets :/ Regions :/ _ -> FrontendRoute_Tweets :/ Regions :/ st
    FrontendRoute_Tweets :/ Refinement :/ _ -> FrontendRoute_Tweets :/ Refinement :/ st
    FrontendRoute_Germa :/ Annot :/ _ ->  FrontendRoute_Germa :/ Annot :/ st
    FrontendRoute_Germa :/ Adjud :/ _ -> FrontendRoute_Germa :/ Adjud :/ st
    FrontendRoute_Germa :/ Regions :/ _ -> FrontendRoute_Germa :/ Regions :/ st
    FrontendRoute_Germa :/ Refinement :/ _ -> FrontendRoute_Germa :/ Refinement :/ st


setTweetSet :: (Routed t AnnotationSettings m, SetRoute t (R FrontendRoute) m, MonadSample t m) => Event t AnnotationSetFilter -> m ()
setTweetSet setFilter = do
  s <- current <$> askSettings
  cur <- sample s
  let newSettings = ffor setFilter $ \ newFilter -> cur & _1 .~ newFilter
  setSettings newSettings



-- | Mapping of annotation state to the respective style class
annotationClass :: Maybe AnnotationState -> T.Text
annotationClass (Just Annotated) = "annotated"
annotationClass (Just NegAnnotated) = "negAnnotated"
annotationClass (Just ImplAnnotated) = "implAnnotated"
annotationClass (Just ImplNegAnnotated) = "implNegAnnotated"
annotationClass Nothing = ""

tweetSetSelection :: (PostBuild t m, DomBuilder t m, MonadHold t m, MonadFix m, Prerender t m, _) => VermerkWidget t m ()
tweetSetSelection = do
  settings <- askSetFilter
  let twSet = ffor settings $ view annset
  annSets <- loadAndRefresh 15 $ tw $ AnnotationSets
  curSel <- sample $ current $ fromMaybe (AnnSetId "-Select annotation set-") <$> twSet
  selEv <- _dropdown_change <$> dropdown curSel (Map.fromList . fmap (\ k -> (k, k ^. coerced)) <$> annSets) def
  setTweetSet $ AnnotationSetFilter . Just <$> selEv

patternSubSwitches :: (PostBuild t m, MonadHold t m, MonadFix m, DomBuilder t m) => Dynamic t (Map.Map PatternId [AnnotationState]) -> Dynamic t (Map.Map PatternId Pattern) -> m (Event t (TweetId -> PrivateTweetApi ()))
patternSubSwitches dannots patterns = do
  dMapE <- listWithKey patterns $ \ patternId dpat -> do
    let disabled = do
          ann <- ffor dannots $ anyOf (ix patternId . traversed . annStateBool) id
          ret <- _retired <$> dpat
          return $ ret && not ann
    patternToggleSwitch patternId dpat (ffor dannots (^? ix patternId . to displayAnnotation)) disabled
  return $ switchDyn $ leftmost . Map.elems <$> dMapE

uncategorized :: (Ord v) => Map.Map c [v] -> Map.Map v d -> Map.Map v d
uncategorized cats = Map.filterWithKey $ \ k _ -> k `notElem` join (Map.elems cats)

patternToggleSwitch :: (DomBuilder t m, PostBuild t m) => PatternId -> Dynamic t Pattern -> Dynamic t (Maybe AnnotationState) -> Dynamic t Bool -> m (Event t (TweetId -> PrivateTweetApi ()))
patternToggleSwitch pid pat isAnnotated disabled =
  do
    act <- yesNoUndecButton "patternToggle flex-row"
           (\ as ->
               case as of
                 Just Annotated -> ""
                 Just ImplAnnotated -> ""
                 _ -> "quasihidden"
           ) (\ as ->
               case as of
                 Just Annotated -> annotationClass as
                 Just ImplAnnotated -> annotationClass as
                 Just NegAnnotated -> annotationClass as
                 Just ImplNegAnnotated -> annotationClass as
                 _ -> "")
           (\ as -> case as of
                      Just NegAnnotated -> ""
                      Just ImplNegAnnotated -> ""
                      _ -> "quasihidden"
           )
           isAnnotated
           (text "✅")
           (fullPattern pid pat)
           (text "❌")

    let ann = \ tid -> Annotate tid pid Annotated
    let negAnn = \ tid -> Annotate tid pid NegAnnotated
    let unAnn = \ tid -> UnAnnotate tid pid
    return $ ffor act $ \case
      Nothing -> unAnn
      Just True -> ann
      Just False -> negAnn


tweetEl selected = elDynClass "span" (tweetClass <$> selected)
  where
    tweetClass True = tweetClass False <> " selected"
    tweetClass False = "tweet"

fullTweet :: (DomBuilder t m, PostBuild t m) => TweetId -> Dynamic t Tweet -> Dynamic t Bool -> m ()
fullTweet tid dt selected = tweetEl selected $ dynText $ ffor dt $ \ t -> show tid ^. packed <> ": " <> t ^. tweet_text


shortTweet :: (DomBuilder t m, PostBuild t m) => Dynamic t Tweet -> Dynamic t Bool -> m ()
shortTweet dt selected = tweetEl selected $ dynText $ ffor dt $ \ t -> t ^. tweet_text


patternToggleMenu :: (DomBuilder t m, MonadFix m, MonadHold t m, PostBuild t m) => Dynamic t (Map.Map PatternId Pattern) -> Dynamic t (Map.Map T.Text [PatternId]) -> Dynamic t (Maybe TweetId) -> Dynamic t (Map.Map PatternId [AnnotationState]) -> VermerkWidget t m ()
patternToggleMenu patterns patternCats mtweet annotations = divClass "flex-row fixed bottom" $ do
  act <- menuLayer
         (do
             pc <- patternCats
             pats <- patterns & mapped %~ Map.keys
             return $ pc & at "all" .~ Just pats
         )
         (id)
         (\ _ dpids -> patternSubSwitches annotations (do
                                                          pids <- dpids
                                                          pats <- patterns
                                                          return $ Map.filterWithKey (\ k p -> k `elem` pids && not (p ^. retired)) pats
                                                      )
         )
  requesting_ $ tw <$> sampleMApp act mtweet

tweetSelection :: (Reflex t, MonadFix m, DomBuilder t m, MonadHold t m, PostBuild t m) =>
  Event t (SelectListAction TweetId) ->
  Map.Map TweetId Tweet ->
  m (Dynamic t (Maybe (TweetId, Tweet)))
tweetSelection keyEvs tweets = do
  (mselected, _) <- divClass "tweetList" $ selectList tweets (\ tid dtw dsel -> (fullTweet tid dtw dsel) >> pure (never, never)) keyEvs
  return mselected

adjudicationButton :: (DomBuilder t m, PostBuild t m, MonadFix m, MonadHold t m) => PatternId -> Dynamic t Pattern -> Dynamic t [AnnotationState] -> Dynamic t Bool -> m (Event t (Bool -> Bool -> TweetId -> PrivateTweetApi ()))
adjudicationButton pid dP state isUndec = do
  act <- yesNoUndecButton "flex-col"
         (\ as ->
             case as of
               Just Annotated -> annotationClass as
               Just ImplAnnotated -> annotationClass as
               _ -> "quasihidden"
         )
         (const "")
         (\ as -> case as of
                    Just NegAnnotated -> annotationClass as
                    Just ImplNegAnnotated -> annotationClass as
                    _ -> "quasihidden"
         )
         (ffor state $ maximumOf traversed)
         (elAttr "span" ("style" =: "font-size: 50%;") $ text "✅")
         (shortPattern pid dP)
         (elAttr "span" ("style" =: "font-size: 50%;") $ text "❌")
  return $ (\ a top undec tweetId -> Adjudicate tweetId pid (a ^. from annStateBool) (Just top) undec) <$> fmapMaybe id act

annotationListDisplay :: (MonadHold t m, DomBuilder t m, MonadFix m, PostBuild t m) => TweetId -> Dynamic t [T.Text] -> Dynamic t (Map.Map T.Text (Map.Map PatternId [Annotation])) -> Dynamic t (Map.Map PatternId Pattern) -> Dynamic t T.Text -> Dynamic t (Map.Map PatternId [GoldAnnotation]) -> Dynamic t Bool -> p1 -> Dynamic t Bool -> VermerkWidget t m ()
annotationListDisplay tid anns annotPatLookup pats goldName goldL sGold sAnnotator hasTL = do
  r <- whenNetworkE sGold $ divClass "goldAnnotations" $ goldRow goldL pats goldName hasTL
  divClass "annotations" $ simpleList anns (annotatorRow annotPatLookup pats)
  requesting_ $ ffor r $ \ rf -> tw $ rf tid
goldRow :: (DomBuilder t m, PostBuild t m, MonadFix m, MonadHold t m) => Dynamic t (Map.Map PatternId [GoldAnnotation]) -> Dynamic t (Map.Map PatternId Pattern) ->  Dynamic t T.Text -> Dynamic t Bool -> m (Event t (TweetId -> PrivateTweetApi ()))
goldRow goldL pats goldName hasTL = divClass "row" $ do
  el "label" $ dynText goldName
  res <- listWithKey pats $
    \ pid dPat -> do
      let ga = ffor goldL (^. at pid)
      let annot =  ffor ga (^.. _Just . traversed . annotationState)
      let disabled = _retired <$> dPat
      let isTop = ffor ga (orOf (_Just . traversed . gTopLevel . _Just))
      let isUndec = ffor ga (orOf (_Just . traversed . gUndecidable))
      let attr = ffor3 disabled isTop isUndec $ \ dis tl undec ->
            if dis then ("style" =: "display: none;")
            else if undec && tl then ("class" =: "undecidable toplevel")
            else if undec then ("class" =: "undecidable")
            else if tl then ("class" =: "toplevel")
            else mempty
      ev <- elDynAttr "div" (attr) $ adjudicationButton pid dPat annot isUndec
      return $ ffor (attachPromptlyDyn hasTL ev) $ \case
        (True, p) -> p False False
        (False, p) -> p False False
  return $ switchDyn $ leftmost . Map.elems <$> res

annotatorRow :: (NotReady t m, DomBuilder t m, MonadHold t m, MonadFix m, PostBuild t m, IsAnnotation a) => Dynamic t (Map.Map T.Text (Map.Map PatternId [a])) -> Dynamic t (Map.Map PatternId Pattern) -> Dynamic t T.Text -> VermerkWidget t m ()
annotatorRow annotPatLookup pats ann =
  let annots = do
        al <- annotPatLookup
        an <- ann
        return $ al ^. ix an
  in whenNetwork (not . Map.null <$> annots) $ divClass "row" $ do
  el "label" $ dynText ann
  annotationButtons pats annots

annotationButtons :: (PostBuild t m, DomBuilder t m, MonadFix m, MonadHold t m, IsAnnotation a) => _ -> Dynamic t (Map.Map PatternId [a]) -> VermerkWidget t m ()
annotationButtons pats patAnnLookup = do
  listWithKey pats $
    \ patternId dPat -> annotationDisplay patternId dPat (ffor patAnnLookup $ view $ ix patternId)
  blank

annotationDisplay :: (PostBuild t m, DomBuilder t m, MonadFix m, MonadHold t m, IsAnnotation a) => PatternId -> Dynamic t Pattern -> Dynamic t [a] -> VermerkWidget t m ()
annotationDisplay patternId dPat annots = do
  let isAnnot = ffor annots $ \ as -> view annotationState <$> (findOf traversed (not . view implicit) as <|> as ^? _head)
  let disabled = ffor dPat $ view retired
  let attr = ffor disabled $ \case
        True -> "style" =: "display: none;"
        _ -> mempty
  let toggleClass = ffor isAnnot $ \ a -> "class" =: annotationClass a
  elDynAttr "span" (attr <> toggleClass) $ shortPattern patternId dPat
  blank
