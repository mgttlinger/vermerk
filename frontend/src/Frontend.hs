{-# LANGUAGE OverloadedStrings, LambdaCase, RecursiveDo, ScopedTypeVariables, TemplateHaskell, FlexibleContexts, TypeFamilies #-}

module Frontend where

import Prelude hiding (tail, filter)

import qualified Data.Text as T

import Obelisk.Frontend
import Obelisk.Route
import Obelisk.Configs
import Obelisk.Route.Frontend
import Obelisk.Generated.Static

import Rhyolite.Api

import Reflex.Network
import Reflex.Dom.Core hiding (preventDefault)
import Reflex.Dom.GadtApi

import Common.Apis
import Common.Route
import Common.Helpers
import Common.Tweets.Api
import qualified Common.Tweets.Types as Tweets
import Common.General.Api
import Common.AnnotationGraph
import Common.Pattern
import Common.Annotation
import Common.Settings
import Common.Auth

import Control.Applicative
import Control.Monad.Identity
import Control.Monad.Reader
import Control.Lens hiding (set, uncons)
import Data.Foldable
import Data.Functor
import Data.List hiding (tail)
import Data.Maybe
import Data.Coerce
import Data.Text.Lens hiding (text)
import qualified Data.Map as Map

import Login
import GeneralWidgets
import Components
import TweetWidgets
import qualified Pages as P

annotationPage :: (MonadHold t m, DomBuilder t m, MonadFix m, PostBuild t m, Prerender t m, SetRoute t (R FrontendRoute) m, PerformEvent t m, TriggerEvent t m, MonadIO (Performable m)) => VermerkWidget t m ()
annotationPage = do
  _ <- tweetSetSelection
  pats <- loadAndRefresh 15 $ gen AllPatterns
  patCats <- loadAndRefresh 60 $ gen PatternCategories
  tweetSet <- fmap _annset <$> askSetFilter
  e <- nowAndUpdated tweetSet
  let tweetSetSelected = fmapMaybe id e
  tweets <- requesting $ tw AllTweets <$ tweetSetSelected
  _ <- runWithReplace (text "Waiting for tweet set to be selected.") $ ffor tweets $
    \case
      Right tws -> do
        annots <- loadAndRefresh 1 $ tw MyAnnotations
        P.annotatePage tws pats patCats annots
      Left err -> text $ "Error occurred while loading tweets: " <> err
  blank


adjudicationPage :: (MonadHold t m, DomBuilder t m, MonadFix m, PostBuild t m, Prerender t m, MonadIO (Performable m), TriggerEvent t m, PerformEvent t m) => VermerkWidget t m ()
adjudicationPage = do
  tabDisplay "tabbar" "activeTab" $ Map.fromList $ Data.List.zip [(0 :: Int)..]
    [ ("Adjudicate", P.adjudicationPage)
    , ("Agreement", P.agreementPage)
    ]

regionAnnotationPage :: (MonadHold t m, DomBuilder t m, MonadFix m, PostBuild t m, Prerender t m, SetRoute t (R FrontendRoute) m) => VermerkWidget t m ()
regionAnnotationPage = do
  _ <- tweetSetSelection
  pat <- patternSelection False
  --tweetMarks <- loadAndRefresh 60 $ Marks s
  loader pat $ \ (pid, dpat) -> do
    slotAnnots <- loadAndRefresh 1 $ tw $ SlotAnnotations pid
    tweets <- loadAndRefresh 120 $ tw $ AnnotatedIn pid
    --gold' <- loadAndRefresh 2 $ goldAnnotations setId
    --let gold = makeGoldLookup <$> gold'
    _ <- paginated 50 tweets $ \ t -> dynNetworkD t $ \ tws -> divClass "tweetList" $
      fst <$> selectList tws (\ tid dt dsel -> do
                                 dynText $ view Tweets.text <$> dt
                                 _ <- networkView $ ffor dsel $ \case
                                   False -> blank
                                   True -> do
                                     el "br" blank
                                     let sAnns = ffor slotAnnots $ \ sa -> sa ^. ix tid
                                     _ <- el "ul" $ listWithKey sAnns $ \ i ans ->  el "li" $ do
                                       ue <- templateSelect (slotExpl dpat) (toList . view Tweets.tokens <$> dt) ans
                                       del <- button "Delete"
                                       requestingJS $ pure $ (tw . ModifySlotAnnotation tid pid i) <$> leftmost [ Nothing <$ del, Just <$> ue ]
                                     na <- addWidget $ mdo
                                       e <- templateSelect (slotExpl dpat) (toList . view Tweets.tokens <$> dt) (pure mempty)
                                       st <- accumDyn (<>) mempty e
                                       return st
                                     _ <- requestingJS $ pure $ (tw . NewSlotAnnotation tid pid) <$> na
                                     blank
                                 return (never, never)
                             ) never
    blank


tokenSelect :: (Adjustable t m, PostBuild t m, MonadHold t m, MonadFix m, DomBuilder t m, Num i) => Dynamic t [a] -> (Dynamic t a -> m ()) -> m (Event t (i, i))
tokenSelect dtokens widget = do
  se <- fmap Map.toList <$> elClass "ul" "tokenList" (
    listViewWithKey (toIxMap <$> dtokens) (\ _ dt -> el "li" $ do
                                              (e, _) <- elClass' "span" "token" (widget dt)
                                              return $ leftmost [ domEvent Mousedown e $> True, domEvent Mouseup e $> False]))
  start <- foldDyn (<|>) Nothing $ fmap fst . find snd <$> se
  return $ attachPromptlyDynWithMaybe (\ ms e -> (\ s -> (fromIntegral s, fromIntegral e)) <$> ms) start (fmapMaybe (fmap fst . find (not . snd)) se)


slotButton :: (DomBuilder t m, PostBuild t m) => Dynamic t Slot -> Dynamic t [T.Text] -> Dynamic t SlotAnnotation -> m (Event t ())
slotButton dsl dtokens ranges = let dslotname = _slotName <$> dsl in do
  let curRange = ffor2 ranges dslotname $ \ r s -> r ^. at s
  (e,_) <- elClass' "span" "slot-filler" $ networkView $ ffor curRange $ \case
    Just (s, e) -> dynText $ T.unwords . take (fromIntegral $ e - s + 1) . drop (fromIntegral s) <$> dtokens
    Nothing -> dynText $ dslotname
  return $ domEvent Click e

templateSelect :: (DomBuilder t m, PostBuild t m, MonadHold t m, MonadFix m) => Template -> Dynamic t [T.Text] -> Dynamic t SlotAnnotation -> m (Event t SlotAnnotation)
templateSelect tpl dtokens slotAnnots = leftmost <$> traverse (el "span" . \case
                                          Left t -> text t $> never
                                          Right s -> mdo
                                            sel <- elDynClass "div" (ffor vis $ \ v -> "popup" <> if v then "" else " hidden") $ tokenSelect dtokens dynText
                                            e <- slotButton (return s) dtokens slotAnnots
                                            vis <- toggle False $ leftmost [e, sel $> ()]
                                            return $ attachPromptlyDynWith (\ oldAnnots se -> oldAnnots & at (s ^. slotName) .~ Just se) slotAnnots sel
                                      ) tpl

refinementPage :: (MonadHold t m, DomBuilder t m, MonadFix m, PostBuild t m, Prerender t m) => VermerkWidget t m ()
refinementPage = do
  annots <- loadAndRefresh 2 $ withAllPatterns $ tw GoldAnnotations
  patternHierarchy <- loadAndRefresh 60 $ gen PatternHierarchy
  patts <- loadAndRefresh 60 $ withAllPatterns $ gen AllPatterns
  pattFilter <- fmap patternFilter <$> askPatternFilter
  let refinableAnnots = do
        chain <- annotChainFromList <$> patternHierarchy
        anns <- annots
        patFil <- pattFilter
        pats <- patts
        return $ Map.mapMaybe (
          \ pas ->
            let enchain = enrich pas chain in
            let r = [ enrichedPath |
                      (patid, as) <- Map.toList pas,
                      patFil patid, -- only unfiltered patterns
                      let das = filter (not . view implicit) as,
                      not $ null das, -- only explicit annotations to start from
                      let forward = any (view $ annotationState . annStateBool) das,
                      let paths = (if forward then lpathsFrom else lpathsTo) patid enchain,
                      enrichedPath <- [ filter (\ (pid, _) -> not $ any id $ pats ^.. ix pid . retired) path | p <- paths, let path = takingEdge (_2 . traversed . annotationState) p],
                      length enrichedPath > 1, -- only none singleton paths
                      any (all (\ a -> a ^. implicit && a ^. annotationState . annStateBool == forward)) (enrichedPath ^.. traversed . _2), -- only paths where can annotate something
                      all (\ a -> all (a /=) (enrichedPath ^.. _last . _2 . traversed . annotationState)) (das ^.. traversed . annotationState) -- only paths that are not already specialised
                    ]
            in if null r then Nothing else Just r) anns
  _ <- paginated 10 refinableAnnots $ \ ra -> divClass "container" $ do
    tweets <- loadOn $ tw . GetTweets . Map.keys <$> updated ra
    listWithKey ra $ \ tid dchains -> do
      divClass "column" $ fullTweet tid (fromJust . Map.lookup tid <$> tweets) (pure False)
      divClass "column" $ do
        simpleList dchains $ \ dpath -> do
          let chainStart = head <$> dpath
          let startAnnot = fromJust . (\ as -> (findOf traversed (not . view implicit) as <|> as ^? _head)) . snd <$> chainStart
          divClass "row" $ simpleList dpath $ \ dnode -> do
            let pid = fst <$> dnode
            let anns = snd <$> dnode
            let annotatable = all (view implicit) <$> anns
            let refineCall = current $ do {
              fromPid <- fst <$> chainStart;
              toPid <- pid;
              return $ tw $ ModifyGoldAnnotation tid fromPid $ Just toPid }
            let alsoCall = current $ do {
              p <- pid;
              as <- view annotationState <$> startAnnot;
              return $ tw $ Adjudicate tid p as Nothing False }
            let noCall = current $ do {
              p <- pid;
              oas <- view (annotationState . oppAnnot) <$> startAnnot;
              return $ tw $ Adjudicate tid p oas (Just False) False
            }
            divClass "column" $ do
              (e, _) <- elDynClass' "span" ((\ b -> if b then "clickable" else "unclickable") <$> annotatable) $ dynNetwork pid $ \ i -> annotationDisplay i (ffor patts $ fromJust . Map.lookup i) anns
              whenNetwork annotatable $ popToggle False (pure "below") $ do
                ref <- button "Refine"
                _ <- requesting_ $ (refineCall <@ ref)
                als <- button "Also"
                _ <- requesting_ $ (alsoCall <@ als)
                no <- button "No"
                _ <- requesting_ $ (noCall <@ no)
                return (leftmost [domEvent Click e, ref, als, no], ()) -- return any event to close the popup
  blank

loggedIn :: (ObeliskWidget t r m) => VermerkWidget t m a -> RoutedT t AnnotationSettings (RequesterT t (ReaderT AnnotationSettings Apis) (Either T.Text) m) ()
loggedIn widget = do
  s <- askSettings
  lin <- lift $ withRequesterT (coerce . flip General . public) id dbLoginWidget
  _ <- runWithReplace blank $ ffor lin $ \ l -> do
    mapRoutedT (\ w -> withRequesterT (logIn l) id $ w) $ widget
  blank

vermerkPage :: forall t m. (ObeliskWidget t (R FrontendRoute) m) => T.Text -> VermerkWidget t m () -> RoutedT t AnnotationSettings m ()
vermerkPage apiEndpoint page =
  do
    settings <- current <$> askSettings
    mapRoutedT (
      \ widget -> mdo
        (_, preRequests) <- runRequesterT widget (responses :: Event t (RequesterData (Either T.Text)))
        let requests = attachWith (\ s -> runIdentity . traverseRequesterData (Identity . flip runReaderT s)) (settings :: Behavior t (AnnotationSettings)) (preRequests :: Event t (RequesterData (ReaderT AnnotationSettings Apis)))
        responses <- performWebSocketRequests apiEndpoint (requests :: Event t (RequesterData Apis))
        blank
      ) $ loggedIn page

landingPage :: ObeliskWidget t (R FrontendRoute) m => m ()
landingPage = do
   el "h1" $ text "Welcome to vermerk!"

   el "h2" $ text "Annotation Modes"
   el "h3" $ text "Tweets"
   el "ul" $ do
     el "li" $ routeLink (FrontendRoute_Tweets :/ Annot :/ def) $ text "Annotation"
     el "li" $ routeLink (FrontendRoute_Tweets :/ Adjud :/ def) $ text "Adjudication"
     el "li" $ routeLink (FrontendRoute_Tweets :/ Regions :/ def) $ text "Region Annotation"
     el "li" $ routeLink (FrontendRoute_Tweets :/ Refinement :/ def) $ text "Annotation Refinement"
   el "h3" $ text "Germapars"
   el "ul" $ do
     el "li" $ routeLink (FrontendRoute_Germa :/ Annot :/ def) $ text "Annotation"
     el "li" $ routeLink (FrontendRoute_Germa :/ Adjud :/ def) $ text "Adjudication"
     el "li" $ routeLink (FrontendRoute_Germa :/ Regions :/ def) $ text "Region Annotation"
     el "li" $ routeLink (FrontendRoute_Germa :/ Refinement :/ def) $ text "Annotation Refinement"
   return ()

-- This runs in a monad that can be run on the client or the server.
-- To run code in a pure client or pure server context, use one of the
-- `prerender` functions.
frontend :: Frontend (R FrontendRoute)
frontend = Frontend
  { _frontend_head = do
      el "title" $ text "vermerk"
      elAttr "link" ("href" =: $(static "milligram.min.css") <> "type" =: "text/css" <> "rel" =: "stylesheet") blank
      elAttr "link" ("href" =: $(static "main.css") <> "type" =: "text/css" <> "rel" =: "stylesheet") blank
  , _frontend_body = do
      let Right enc = checkEncoder fullRouteEncoder
      mhost <- getTextConfig "common/route"
      case mhost of
        Nothing -> text "Configuration is missing!"
        Just host -> do
          let apiEndpoint = (T.replace "http" "ws" host) <> (renderBackendRoute enc $ BackendRoute_Api :/ ())
          subRoute_ $ \case
            FrontendRoute_Main -> landingPage
            FrontendRoute_Tweets ->
              subRoute_ $ \case
                 Annot -> vermerkPage apiEndpoint $ annotationPage
                 Adjud -> vermerkPage apiEndpoint $ adjudicationPage
                 Regions -> vermerkPage apiEndpoint $ regionAnnotationPage
                 Refinement -> vermerkPage apiEndpoint $ refinementPage
  }
