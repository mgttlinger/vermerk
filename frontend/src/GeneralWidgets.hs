{-# LANGUAGE FlexibleContexts, LambdaCase, TypeFamilies, ConstraintKinds, RecursiveDo, OverloadedStrings #-}

module GeneralWidgets where

import Components

import Obelisk.Route.Frontend
import Reflex.Dom
import Reflex.Network

import Common.Apis
import Common.Settings
import Common.Pattern
import Common.General.Api

import Control.Lens
import Control.Monad.Identity
import Control.Monad.IO.Class

import Data.Map
import Data.Text
import Data.Text.Lens

type VermerkWidget t m = RoutedT t AnnotationSettings (RequesterT t Deferred (Either Text) m)

askSettings :: (Routed t AnnotationSettings m) => m (Dynamic t AnnotationSettings)
askSettings = askRoute

askSetFilter :: (Routed t AnnotationSettings m, Functor m, Reflex t) => m (Dynamic t AnnotationSetFilter)
askSetFilter = fmap (view _1) <$> askSettings

askPatternFilter :: (Routed t AnnotationSettings m, Functor m, Reflex t) => m (Dynamic t PatternFilter)
askPatternFilter = fmap (view _2) <$> askSettings

patternInfo :: (DomBuilder t m, MonadHold t m, Adjustable t m, MonadFix m) => Event t [PatternId] -> VermerkWidget t m (Event t (Map PatternId Pattern))
patternInfo patts = await "pattern info" =<< requesting (gen . GetPatterns <$> patts)

getPatternInfo :: (PostBuild t m, PerformEvent t m, TriggerEvent t m, MonadIO (Performable m), MonadHold t m) => Dynamic t [PatternId] -> VermerkWidget t m (Dynamic t (Map PatternId Pattern))
getPatternInfo patts = do
  pattsTrigger <- nowAndUpdated patts
  info <- requesting $ gen . GetPatterns <$> pattsTrigger
  holdDynMonoid info

shortPattern :: (DomBuilder t m, PostBuild t m) => PatternId -> Dynamic t Pattern -> m ()
shortPattern pid dp = elDynAttr "span" (ffor dp $ \ p -> "title" =: p ^. name <> "class" =: "pattern") $ dynText $ ffor dp $ \ p -> show pid ^. packed

fullPattern :: (DomBuilder t m, PostBuild t m) => PatternId -> Dynamic t Pattern -> m ()
fullPattern pid dp = elDynAttr "span" (ffor dp $ \ p -> "title" =: p ^. explanation <> "class" =: "pattern") $ dynText $ ffor dp $ \ p -> show pid ^. packed <> ": " <> p ^. name

getPatterns :: (MonadFix m, Prerender t m) => VermerkWidget t m (Event t (Either Text (Map PatternId Pattern)))
getPatterns = getOnLoaded $ gen AllPatterns

patternSelection :: (DomBuilder t m, MonadFix m, MonadHold t m, Prerender t m, PostBuild t m) => Bool -> VermerkWidget t m (Dynamic t (Maybe (PatternId, Pattern)))
patternSelection includeRetired = do
  patternsReady <- await "patterns" =<< getPatterns
  res <- networkHold (pure $ constDyn Nothing) $ ffor patternsReady $
    \ patts -> do
      let patternOptions = Data.Map.filter (\ p -> includeRetired || not (p ^. retired)) patts
      rec c <- elDynAttr "div" ((\ p -> mempty & at "title" .~ (view (_2 . explanation) <$> p)) <$> c) $ contentDropdown (constDyn patternOptions) $ \ pid p -> show pid ^. packed <> ": " <> p^.name
      return c
  return $ join res

-- patternCategories :: m (Dynamic t (Map Text [PatternId]))

-- patternHierarchy :: m (Dynamic t [(PatternId, PatternId)])
