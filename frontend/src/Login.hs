{-# LANGUAGE RecursiveDo, LambdaCase, OverloadedStrings #-}

module Login where

import Reflex.Dom

import Control.Lens
import Control.Monad.Identity

import Data.Text (Text)
import qualified Data.Text as T

import Common.Auth
import Common.General.Api

import Components

dbConfig :: (DomBuilder t m) => m (Dynamic t Login)
dbConfig = do
  u <- fmap T.unpack . value <$> labelled "DB user" (inputElement def)
  pw <- fmap T.unpack . value <$> labelled "Password" (inputElement $ def & inputElementConfig_elementConfig . elementConfig_initialAttributes .~ (AttributeName Nothing "type" =: "password"))
  return $ Login <$> u <*> pw

data LoginState = NotTried
                | Failed Text
                | LoggedIn Login
                deriving (Eq, Show)

whenFailed :: (Applicative m) => LoginState -> (Text -> m ()) -> m ()
whenFailed (Failed s) f = f s
whenFailed _ _ = pure ()

dbLoginWidget :: (DomBuilder t m, MonadFix m, PostBuild t m, MonadHold t m) => RequesterT t PublicApi (Either Text) m (Event t Login)
dbLoginWidget = do
  rec e <- dynNetworkE state $ \case
        LoggedIn _ -> return never
        other -> divClass "container" $ do
          (d, dl) <- elClass' "div" (case other of
                                       NotTried -> ""
                                       Failed _ -> "loginFailed"
                                    ) $ dbConfig
          sel <- button "Login"
          whenFailed other $ \ t -> text $ "Login failed! " <> t
          res <- fmap (either (const $ Just "request failed") id) <$> (requesting $ CheckLogin <$> (current dl <@ leftmost [sel, keydown Enter d]))
          return $ ffor (attachPromptlyDyn dl res) $ \case
            (lin, Nothing) -> LoggedIn lin
            (_, Just err) -> Failed err
      state <- holdDyn NotTried $ e
  return $ fforMaybe (updated state) $ \case
    LoggedIn lin -> Just lin
    _ -> Nothing
