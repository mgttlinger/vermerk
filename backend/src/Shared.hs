{-# LANGUAGE OverloadedStrings, RecordWildCards, StandaloneDeriving, DerivingStrategies, ScopedTypeVariables, ConstraintKinds, FlexibleContexts, DeriveGeneric, DeriveAnyClass, GeneralisedNewtypeDeriving, Rank2Types, TypeSynonymInstances, FlexibleInstances, MultiParamTypeClasses, TypeOperators #-}

module Shared where

import Obelisk.Configs

import Common.Auth
import Common.Pattern
import Common.Annotation
import Common.AnnotationSet
import Common.Settings

import Prelude hiding (fail)

import Control.Monad.IO.Class
import Control.Monad.Except
import Control.Monad.Reader hiding (fail)
import Control.Exception
import Control.Lens

import Data.Aeson
import Data.Text (Text)
import qualified Data.Text as T
import Data.Map

import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToField
import Database.PostgreSQL.Simple.FromField
import Database.PostgreSQL.Simple.Range

import GHC.Generics (Generic)
import GHC.Word

data DBConfig = DBConf { host :: String
                       , port :: Word16
                       , database :: String
                       } deriving (Eq, Show, Read, Generic, FromJSON, ToJSON)


connectDB :: MonadReader Login m => DBConfig -> m ConnectInfo
connectDB DBConf{..} = do
  Login{..} <- ask
  return $ ConnectInfo host port _user _password database

deriving newtype instance FromField PatternId
deriving newtype instance ToField PatternId

instance FromRow PatternId where
  fromRow = PatternId <$> field

deriving newtype instance FromField AnnotationSetId
deriving newtype instance ToField AnnotationSetId

instance FromField AnnotationState where
  fromField s meta = view (from annStateBool) <$> fromField s meta
instance ToField AnnotationState where
  toField = toField . view annStateBool

deriving instance FromRow AnnotationSetId
deriving instance FromRow Annotation
deriving instance FromRow GoldAnnotation

toPGRange (a, b) = PGRange (Inclusive a) (Inclusive b)

dbMap :: (Ord k) => [k :. v] -> Map k v
dbMap = fromList . fmap (\ (a :. b) -> (a, b))

getJsonConfig :: (HasConfigs m, MonadError Text m, FromJSON a) => Text -> m a
getJsonConfig path = do
  cfg <- maybe (throwError $ path <> " config missing") pure =<< getConfig path
  liftEither $ eitherDecodeStrict cfg & _Left %~ T.pack

instance HasConfigs m => HasConfigs (ExceptT e m)

class (MonadIO m, MonadError Text m) => DBAction m where
  askConnection :: m Connection
  askSetFilter :: m AnnotationSetFilter
  askPatternFilter :: m PatternFilter

  q :: (ToRow q, FromRow r) => Query -> q -> m [r]
  q qstr args = do
    db <- askConnection
    liftIO $ print =<< formatQuery db qstr args
    liftIO $ query db qstr args

  q_ :: (FromRow r) => Query -> m [r]
  q_ qstr = do
    db <- askConnection
    liftIO $ query_ db qstr

  e :: (ToRow q) => Query -> q -> m ()
  e qstr args = do
    db <- askConnection
    liftIO $ print =<< formatQuery db qstr args
    liftIO $ execute db qstr args
    return ()

  eM :: (ToRow q) => Query -> [q] -> m ()
  eM qstr args = do
    db <- askConnection
    liftIO $ print =<< formatMany db qstr args
    liftIO $ executeMany db qstr args
    return ()


newtype DBActionR a = DBActionR { runDBAction :: Connection -> AnnotationSetFilter -> PatternFilter -> ExceptT Text IO a }

instance Functor DBActionR where
  fmap f a = DBActionR $ \ con asf pf -> f <$> runDBAction a con asf pf

instance Applicative DBActionR where
  pure a = DBActionR $ const $ const $ const $ pure a
  ff <*> fa = DBActionR $ \ con asf pf -> runDBAction ff con asf pf <*> runDBAction fa con asf pf

instance Monad DBActionR where
  ma >>= f = DBActionR $ \ con asf pf -> do
    a <- runDBAction ma con asf pf
    runDBAction (f a) con asf pf

instance MonadIO DBActionR where
  liftIO = DBActionR . const . const . const . liftIO

instance MonadError Text DBActionR where
  throwError = DBActionR . const . const . const . throwError
  catchError ma handler = DBActionR $ \ con asf pf -> (runDBAction ma con asf pf) `catchError` ((\ mr -> runDBAction mr con asf pf) . handler)

instance DBAction DBActionR where
  askConnection = DBActionR $ \ conn _ _ -> pure conn
  askSetFilter = DBActionR $ \ _ asf _ -> pure asf
  askPatternFilter = DBActionR $ \ _ _ pf -> pure pf

withConfigPostgres :: (MonadIO m, HasConfigs m, MonadError Text m) => Login -> AnnotationSettings -> DBActionR a -> m a
withConfigPostgres l (asf, pf) f = do
  tdbConf <- getJsonConfig "backend/tweet_db"
  let cinfo = connectDB tdbConf l
  res <- liftIO $ bracket
    (connect cinfo)
    close
    (\ db -> runExceptT $ runDBAction f db asf pf)
  liftEither res

parseAnnotationState :: Bool -> Text -> AnnotationState
parseAnnotationState True t = if "impl" `T.isInfixOf` t then ImplAnnotated else Annotated
parseAnnotationState False t = if "impl" `T.isInfixOf` t then ImplNegAnnotated else NegAnnotated

parseGoldAnnotation (tid, pid, b, t, tl, undec, an, c) = (tid, (pid, GAnn (parseAnnotationState b t) tl undec an c))
parseAnnotation (tid, pid, an, m, b, t, c) = (tid, (an, (pid, (Ann m (parseAnnotationState b t) an c))))
parseMyAnnotation (tid, pid, b, t) = (tid, (pid, parseAnnotationState b t))
