{-# LANGUAGE OverloadedStrings, StandaloneDeriving, FlexibleInstances, MultiParamTypeClasses, FlexibleContexts, GADTs, DeriveAnyClass, DerivingStrategies, GeneralisedNewtypeDeriving
, TemplateHaskell, LambdaCase, DerivingVia #-}

module Tweets(handleTweetApi) where

import Obelisk.Configs

import Shared
import qualified General as G

import Common.Helpers
import Common.Tweets.Api
import Common.Tweets.Types
import Common.Pattern
import Common.Annotation
import Common.General.Api
import Common.Settings
import Common.Confusion
import Common.Range
import Common.Auth

import Control.Monad.IO.Class
import Control.Monad.Except
import Control.Lens

import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.ToField
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.FromField
import Database.PostgreSQL.Simple.Range

import Data.Aeson
import Data.Text (Text)
import Data.List (nub)
import Data.Vector.Lens
import Data.Int
import qualified Data.Map as M
import Data.Either

deriving newtype instance FromField TweetId
deriving newtype instance ToField TweetId

instance FromRow TweetId where
  fromRow = field

handlePrivateApi :: (DBAction m) => PrivateTweetApi r -> m r
handlePrivateApi (GetTweets idxs) = do
  vs <- q "select idx, author, screen_name, text, text_normalized, tokens from rant.tweets where idx in ?" $ Only $ In idxs
  return $ M.fromList $ (\ (i, a, s, t, tn, tk) -> (i, Tweet a s t tn tk)) <$> vs
handlePrivateApi (AllTweets) = do
  s <- askSetFilter
  vs <- if s ^. filters
    then q "select idx, author, screen_name, text, text_normalized, tokens from rant.tweets as A inner join rant.virtual_tweet_sets as S on A.idx = Any(S.tweets) where S.set_name = ?" $ Only (s ^. annset)
    else q_ "select idx, author, screen_name, text, text_normalized, tokens from rant.tweets"
  return $ M.fromList $ (\ (i, a, s, t, tn, tk) -> (i, Tweet a s t tn tk)) <$> vs
handlePrivateApi (RetrieveDisagreeing) = do
  s <- askSetFilter
  f <- askPatternFilter
  vs <- if s ^. filters
    then q "select distinct t.idx, t.author, t.screen_name, t.\"text\" , t.text_normalized, t.tokens from rant.classification c left join rant.\"classification_gold+\" cg on cg.tweet = c.tweet and cg.pattern = c.pattern left join rant.tweets t on c.tweet = t.idx inner join rant.virtual_tweet_sets S on t.idx = Any(S.tweets) where cg.annotation is distinct from c.annotation and c.annotation is not null and c.manual and S.set_name = ?" $ Only (s ^. annset)
    else q "select distinct t.idx, t.author, t.screen_name, t.\"text\" , t.text_normalized, t.tokens from rant.\"classification+\" c left join rant.\"classification_gold+\" cg on cg.tweet = c.tweet and cg.pattern = c.pattern left join rant.tweets t on c.tweet = t.idx where cg.annotation is distinct from c.annotation and c.annotation is not null and (? or c.pattern in ?) and c.manual" $ (f ^. filters, In $ f ^. patterns)
  return $ M.fromList $ (\ (i, a, s, t, tn, tk) -> (i, Tweet a s t tn tk)) <$> vs
handlePrivateApi (AnnotationSets) = q_ "select set_name from rant.virtual_tweet_sets"
handlePrivateApi (Annotate tid pid as) = do
  e "insert into rant.classification(tweet, pattern, manual, annotation) values (?, ?, ?, ?) on conflict on constraint classification_un do update set annotation = ?, annotated_at = now()" $ (tid, pid, True, as, as)
handlePrivateApi (UnAnnotate tid pid) = do
  e "delete from rant.classification where tweet = ? and pattern = ? and annotator = \"current_user\"()" $ (tid, pid)
handlePrivateApi (Adjudicate le re as tl undec) = do
  e "insert into rant.classification_gold(tweet, pattern, annotation, toplevel, undecidable) values (?, ?, ?, ?, ?)" $ (le, re, as, tl, undec)
handlePrivateApi (ModifyGoldAnnotation tid pid (Just newpid)) = do
  e "update rant.classification_gold set pattern = ? where tweet = ? and pattern = ?" (newpid, tid, pid)
handlePrivateApi (ModifyGoldAnnotation tid pid Nothing) = do
  e "delete from rant.classification_gold where tweet = ? and pattern = ?" (tid, pid)
handlePrivateApi (ManualAnnotations) = do
  s <- askSetFilter
  f <- askPatternFilter
  res <- if s ^. filters
         then q "select tweet, pattern, annotator, manual, annotation, via, comment from rant.\"classification+\" as A inner join rant.virtual_tweet_sets as S on A.tweet = Any(S.tweets) where S.set_name = ? and (? or pattern in ?) and manual" (s ^. annset, f ^. filters, In $ f ^. patterns)
         else q "select tweet, pattern, annotator, manual, annotation, via, comment from rant.\"classification+\" as A where (? or pattern in ?) and manual" $ (f ^. filters, In $ f ^. patterns)
  return $ makeLookup $ parseAnnotation <$> res
handlePrivateApi (DisagreeingManualAnnotations) = do
  anns <- handlePrivateApi (ManualAnnotations)
  golds <- handlePrivateApi (GoldAnnotations)
  return $ M.filterWithKey (\ tid annorPatLookup ->
                              let gs pid = golds ^. ix tid . ix pid in
                              ianyOf (traversed . itraversed) (\ pid ass -> any (\ ann -> all (\ gann -> gann ^. annotationState . annStateBool /= ann ^. annotationState . annStateBool) (gs pid)) ass) annorPatLookup) anns
handlePrivateApi (MyAnnotations) = do
  s <- askSetFilter
  f <- askPatternFilter
  res <- if s ^. filters
         then q "select tweet, pattern, annotation, via from rant.\"classification+\" as A inner join rant.virtual_tweet_sets as S on A.tweet = Any(S.tweets) where annotator = \"current_user\"() and S.set_name = ? and (? or pattern in ?)" (s ^. annset, f ^. filters, In $ f ^. patterns)
         else q "select tweet, pattern, annotation, via from rant.\"classification+\" as A where annotator = \"current_user\"() and (? or pattern in ?)" $ (f ^. filters, In $ f ^. patterns)
  return $ fmap lookupFromTuples $ lookupFromTuples $ parseMyAnnotation <$> res
handlePrivateApi (GoldAnnotations) = do
  s <- askSetFilter
  f <- askPatternFilter
  res <- if s ^. filters
         then q "select tweet, pattern, annotation, via, toplevel, undecidable, adjudicated_by, comment from rant.\"classification_gold+\" as A inner join rant.virtual_tweet_sets as S on A.tweet = Any(S.tweets) where S.set_name = ? and (? or pattern in ?)" (s ^. annset, f ^. filters, In $ f ^. patterns)
         else q "select tweet, pattern, annotation, via, toplevel, undecidable, adjudicated_by, comment from rant.\"classification_gold+\" where (? or pattern in ?)" (f ^. filters, In $ f ^. patterns)
  return $ makeGoldLookup $ parseGoldAnnotation <$> res
handlePrivateApi (NeedAdjudication) = do
  s <- askSetFilter
  f <- askPatternFilter
  if s ^. filters
    then q "select tweet from rant.needs_adjudication inner join rant.virtual_tweet_sets as S on tweet = Any(S.tweets) where S.set_name = ? and (? or (patterns && (?)::bigint[]))" (s ^. annset, f ^. filters, f ^. patterns . vector)
    else q "select tweet from rant.needs_adjudication where (? or (patterns && (?)::bigint[]))" (f ^. filters, f ^. patterns . vector)
handlePrivateApi (HasTopLevel) = do
  s <- askSetFilter
  if s ^. filters
    then q "select tweet from rant.needs_toplevel where not has_toplevel and ? = Any(set_names)" (Only $ s ^. annset)
    else q_ "select tweet from rant.needs_toplevel where not has_toplevel"
handlePrivateApi (Confusions) = do
  f <- askPatternFilter
  anns <- handlePrivateApi (ManualAnnotations)
  pats <- M.keys . M.filterWithKey (\ k v -> patternFilter f k && (not $ _retired v)) <$> G.handlePrivateApi (AllPatterns)
  let dats = nub $ anns ^.. itraversed . asIndex
  let annotators = nub $ anns ^.. traversed . itraversed . asIndex
  let annots = anns
  let annotated annor pat tweet = anyOf (ix tweet . ix annor . ix pat . folded . annotationState . annStateBool) id $ annots
  return $ M.fromList [ (p :: PatternId, M.fromList [ ((a1, a2), confuseCount (annotated a1 p) (annotated a2 p) dats) | (a1, a2) <- upairs annotators, a1 /= a2 ]
             ) | p <- pats ]
handlePrivateApi (GoldConfusions) = do
  f <- askPatternFilter
  anns <- handlePrivateApi (ManualAnnotations)
  goldAnns <- handlePrivateApi (GoldAnnotations)
  pats <-  M.keys . M.filterWithKey (\ k v -> patternFilter f k && (not $ _retired v)) <$> G.handlePrivateApi (AllPatterns)
  let dats = nub $ M.keys anns <> M.keys goldAnns
  let annotators = nub $ anns  ^.. traversed . itraversed . asIndex
  let annotated annor pat tweet = anyOf (ix tweet . ix annor . ix pat . folded . annotationState . annStateBool) id $ anns
  return $ M.fromList [ (p, M.fromList [ (a, confuseCount (\ tw -> anyOf (ix tw . ix p . folded . annotationState . annStateBool) id goldAnns) (annotated a p) dats) | a <- annotators ]) | p <- pats ]
handlePrivateApi (AnnotatedIn pid) = do
  s <- askSetFilter
  res <- if s ^. filters
    then q "select idx, author, screen_name, text, text_normalized, tokens from rant.tweets inner join rant.classification_gold as A on idx = A.tweet inner join rant.virtual_tweet_sets as S on A.tweet = Any(S.tweets) where S.set_name = ? and pattern = ? and annotation" (s ^. annset, pid)
    else q "select idx, author, screen_name, text, text_normalized, tokens from rant.tweets inner join rant.classification_gold as A on idx = A.tweet where pattern = ? and annotation" $ Only pid
  return $ M.fromList $ (\ (i, a, s, t, tn, tk) -> (i, Tweet a s t tn tk)) <$> res
handlePrivateApi (SlotAnnotations pid) = do
  s <- askSetFilter
  res <- if s ^. filters
         then q "select sa.idx, sa.tweet, json_object_agg(slot,area) as slots from rant.slot_annotation sa join rant.virtual_tweet_sets S on sa.tweet = Any(S.tweets) where S.set_name = ? and sa.pattern = ? group by sa.idx, sa.tweet, sa.pattern" (s ^. annset, pid)
         else q "select sa.idx, sa.tweet, json_object_agg(slot,area) as slots from rant.slot_annotation sa where sa.pattern = ? group by sa.idx, sa.tweet, sa.pattern" $ Only pid
  return $ collectIntoMap (view _2) (\ t -> mempty & at (t^._1) .~ Just (case fromJSON (t^._3) of Success a -> (fromRight (0,0) . parseText rangeParser <$>  a :: SlotAnnotation))) (res :: [(Int32, TweetId, Value)])
handlePrivateApi (NewSlotAnnotation tid pid sa) = do
  res <- q_ "select nextval('rant.slot_annotation_idx_seq')"
  let nid = case head res of
        Only i -> i
  eM "insert into rant.slot_annotation(idx, tweet, pattern, slot, area) VALUES (?, ?, ?, ?, ?)" (flip fmap (M.toList sa) $ \ (k, v) -> (nid :: Int, tid, pid, k, toPGRange v))
handlePrivateApi (ModifySlotAnnotation tid pid idx Nothing) = e "delete from rant.slot_annotation where tweet = ? and pattern = ? and idx = ?" (tid, pid, idx)
handlePrivateApi (ModifySlotAnnotation tid pid idx (Just sa)) = itraverse_ (\ k v -> e "update rant.slot_annotation set area = ? where slot = ? and tweet = ? and pattern = ? and idx = ?" (toPGRange v, k, tid, pid, idx)) sa

handleTweetApi :: (MonadIO m, HasConfigs m) => AnnotationSettings -> Login -> PrivateTweetApi a -> ExceptT Text m a
handleTweetApi s l r = withConfigPostgres l s $ handlePrivateApi r
