{-# LANGUAGE OverloadedStrings, GADTs, FlexibleContexts, StandaloneDeriving, DeriveAnyClass #-}

module General (handleGeneralApi, handlePrivateApi) where

import Shared

import Rhyolite.Api
import Obelisk.Configs

import Common.General.Api
import Common.Helpers
import Common.Settings
import Common.Pattern

import Control.Monad.IO.Class
import Control.Monad.Except
import Control.Lens

import Database.PostgreSQL.Simple
import Data.Text (Text)
import qualified Data.Map as M

deriving instance FromRow Pattern

handlePublicApi :: (MonadIO m, HasConfigs m, MonadError Text m) => AnnotationSettings -> PublicApi r -> m r
handlePublicApi s (CheckLogin l) = flip catchError (pure . Just) $ withConfigPostgres l s $ pure Nothing

handlePrivateApi :: (DBAction m) => PrivateApi r -> m r
handlePrivateApi (GetPatterns idxs) = do
  vs <- q "select idx, name, template, explanation, retired from rant.patterns where idx in ?" $ Only $ In idxs
  return $ dbMap vs
handlePrivateApi (AllPatterns) = do
  f <- askPatternFilter
  vs <- q "select idx, name, template, explanation, retired from rant.patterns where ? or idx in ?" $ (f ^. filters, In $ f ^. patterns)
  return $ dbMap vs
handlePrivateApi PatternCategories = do
  categorised <- q_ "select category, pattern from rant.\"pattern_category+\""
  return $ collectIntoMap fst (pure . snd) categorised
handlePrivateApi PatternHierarchy = q_ "select general, special from rant.pattern_specialisation"

handleGeneralApi :: (MonadIO m, HasConfigs m) => AnnotationSettings -> GeneralApi a -> ExceptT Text m a
handleGeneralApi s (ApiRequest_Public r) = handlePublicApi s r
handleGeneralApi s (ApiRequest_Private l r) = withConfigPostgres l s $ handlePrivateApi r
