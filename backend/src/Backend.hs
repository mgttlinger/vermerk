{-# LANGUAGE LambdaCase, GADTs, OverloadedStrings, FlexibleInstances, MultiParamTypeClasses, FlexibleContexts, TypeApplications, RankNTypes, ScopedTypeVariables #-}

module Backend where

import General
import Tweets
import Germaparl

import Common.Route
import Common.Apis

import Obelisk.Route hiding (encode, decode)
import Obelisk.Backend
import Obelisk.Configs
import qualified Obelisk.ExecutableConfig.Lookup as Lookup
import Rhyolite.Backend.WebSocket

import Snap
import Control.Monad.Reader
import Control.Monad.Except
import Control.Exception
import Control.Lens
import Data.Text (Text, pack)
import Reflex.Dom.GadtApi.WebSocket

import Database.PostgreSQL.Simple

-- | Error page using http.cat to visualize the status code
catRes :: Int -> Snap () -> Snap ()
catRes i cnt = do
  modifyResponse $ setResponseCode i
  modifyResponse $ setHeader "Content-Type" "text/html"
  writeText $ "<body style='background: black; color: white;'><img style='display: block; margin-left:auto; margin-right:auto;' src='https://http.cat/status/" <> pack (show i) <> "' />"
  cnt
  writeText "</body>"

-- | Error page for a 404
err404 :: Snap ()
err404 = catRes 404 $ return ()

backend :: Backend BackendRoute FrontendRoute
backend = Backend
  { _backend_run = \serve -> do
      serve $ \case
        (BackendRoute_Missing :/ ()) -> err404
        (BackendRoute_Api :/ ()) -> do
          -- (sn, fin) <- serveVessel _ handleTweetAnnotationApi _ _
          -- forever sn
          -- fin

          withWebsocketsConnection $ \ conn -> do
            forever $ do
              dm <- getDataMessage conn 
              cfg <- Lookup.getConfigs -- load configs
              let er = (mkTaggedResponse dm $ \case
                           General s req -> handleGeneralApi s req
                           Tweet s l req -> handleTweetApi s l req
                           Germaparl s l req -> handleGermaparlApi s l req
                       ) :: ExceptT Text (ConfigsT IO) (Either String TaggedResponse) -- handle tweet requests feeding in config
              r <- (runConfigsT cfg (join . (_Right . _Left %~ pack) <$> runExceptT er)) `catches` [ Handler (\ (e :: IOException) -> pure $ Left $ pack $ displayException e)
                                                                                                   , Handler (\ (e :: SqlError) -> pure $ Left $ pack $ displayException e)
                                                                                                   ]
              case r of
                Left err -> sendEncodedDataMessage conn err -- reply with failure message if request handling failed
                Right rsp -> sendEncodedDataMessage conn rsp -- send back response

              pure ()
  , _backend_routeEncoder = fullRouteEncoder
  }
