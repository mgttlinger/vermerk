{-# LANGUAGE OverloadedStrings, StandaloneDeriving, FlexibleInstances, MultiParamTypeClasses, FlexibleContexts, GADTs, DeriveAnyClass, DerivingStrategies, GeneralisedNewtypeDeriving
, TemplateHaskell, LambdaCase, DerivingVia #-}

module Germaparl (handleGermaparlApi) where

import Obelisk.Configs

import Shared
import qualified General as G

import Common.Helpers
import Common.Germaparl.Api
import Common.Germaparl.Types
import Common.Auth
import Common.Settings

import Control.Monad.IO.Class
import Control.Monad.Except
import Control.Lens

import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.ToField
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.FromField

import Data.Vector.Lens
import qualified Data.Map as M
import Data.Text (Text)

deriving newtype instance FromField SentenceId
deriving newtype instance ToField SentenceId

instance FromRow SentenceId where
  fromRow = field

deriving instance FromField Sentence
deriving instance FromRow Sentence

handlePrivateApi :: (DBAction m) => PrivateGermaApi r -> m r
handlePrivateApi (GetSentences idxs) = do
  vs <- q "select cwbid, word, tokens from rant.germaparl where idx in ?" $ Only $ idxs ^. vector
  return $ M.fromList vs
handlePrivateApi (AllSentences) = do
  s <- askSetFilter
  vs <- if s ^. filters
    then q "select cwbid, word, tokens from rant.germaparl as A inner join rant.virtual_tweet_sets as S on A.idx = Any(S.tweets) where S.set_name = ?" $ Only (s ^. annset)
    else q_ "select cwbid, word, tokens from rant.germaparl"
  return $ M.fromList vs

handleGermaparlApi :: (MonadIO m, HasConfigs m) => AnnotationSettings -> Login -> PrivateGermaApi a -> ExceptT Text m a
handleGermaparlApi s l r = withConfigPostgres l s $ handlePrivateApi r
