{ pkgs ? import <nixpkgs> {} }:

let
  vermerk = import ./default.nix {};
  vermerk-config = pkgs.copyPathToStore ./config;
in
pkgs.dockerTools.buildImage {
  name = "vermerkDocker";
  tag = "latest";

  created = "now";
  fromImageName = "alpine";
  fromImageTag = "latest";

  contents = [ vermerk.exe vermerk-config ];
  runAsRoot= ''
    #!${pkgs.runtimeShell}
    mkdir -p /vermerk
    cp -r "${vermerk.exe.out}"/* /vermerk/
  '';

  config = {
    Entrypoint = [ "/vermerk/backend" ];
    WorkingDir = "/vermerk";
    ExposedPorts = {
      "8000/tcp" = {};
    };
  };
}
