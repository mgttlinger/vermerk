{ system ? builtins.currentSystem
, obelisk ? import ./.obelisk/impl {
    inherit system;
    iosSdkVersion = "13.2";

    # You must accept the Android Software Development Kit License Agreement at
    # https://developer.android.com/studio/terms in order to build Android apps.
    # Uncomment and set this to `true` to indicate your acceptance:
    # config.android_sdk.accept_license = false;

    # In order to use Let's Encrypt for HTTPS deployments you must accept
    # their terms of service at https://letsencrypt.org/repository/.
    # Uncomment and set this to `true` to indicate your acceptance:
    # terms.security.acme.acceptTerms = false;
  }
}:
with obelisk;
project ./. ({ pkgs, hackGet, ... }@args: {
  android.applicationId = "mgttlinger.vermerk";
  android.displayName = "Vermerk";
  ios.bundleIdentifier = "mgttlinger.vermerk";
  ios.bundleName = "Vermerk";
  overrides = pkgs.lib.composeExtensions (pkgs.callPackage (hackGet ./dep/rhyolite) args).haskellOverrides
    (super: self: {
      reflex-gadt-api = self.callCabal2nix "reflex-gadt-api" ./dep/reflex-gadt-api {};
      # monad-logger-extras = self.callHackage "monad-logger-extras" "0.1.1.1" {};
      # commutative-semigroups = self.callCabal2nix "commutative-semigroups" ./dep/commutative-semigroups {};
      # beam-migrate = super.beam-migrate_0_5_2_0;
      # monoid-subclasses = self.callHackage "monoid-subclasses" "1.1" {};
      # universe-some = self.callHackage "universe-some" "1.2.1" {};
      # universe-base = self.callHackage "universe-base" "1.1.1" {};
      # QuickCheck = self.callHackage "QuickCheck" "2.13.1" {};
    });
})
